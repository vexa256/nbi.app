<div class="col-md-12">

   <div class="card card-default shadow-lg">
          <div class="card-header">
            <h3 class="card-title p-2">Project Activity Expenditure  Report's Critical Variables
            </h3>


            <table  class="table table-bordered  " >

                <thead>
                  <tr >
                    <th class="bg-dark text-light">Category</th>
                    <th class="bg-dark text-light">Year</th>
                    <th class="bg-warning text-light">Project</th>
                    <th class="bg-primary text-light">Activity</th>
                    <th class="bg-danger text-light">Total Expenditure  (total for all debits and credits in USD)</th>

                  </tr>
                </thead>

                <tbody>
                  <tr >

                    <td class="bg-dark text-light">{{  $Category }} </td>
                    <td class="bg-dark text-light">{{  $Year }} </td>
                    <td class="bg-warning text-light">{{ $Project}} </td>
                    <td class="bg-primary text-light">{{ $Activity}} </td>
                    <td class="scrner_s bg-danger text-light"></td>


                  </tr>
                </tbody>

            </table>


</div>
</div>
</div>
<div class="col-md-12">

   <div class="card card-default shadow-lg">
          <div class="card-header">
            <h3 class="card-title p-2">Project Activity Expenditure Report Transaction Logs
            </h3>

          </div>
          <div class="card-body">


            <table  class="table data table-bordered  " >

                <thead>
                  <tr >

                    <th class="">Posting Date</th>
                    <th class="">Description</th>
                    <th class="bg-dark text-light shadow-lg">Amount (USD)</th>
                    <th class="">Payee</th>
                    <th class="">Donor</th>

                  </tr>
                </thead>

                <tbody>
                   @isset($Selections)
                        @foreach ($Selections as $data)
                  <tr >

                    <?php

$a = number_format($data->{'Amount'}, 2);

$b = $a;?>

      <input type="text" class="d-none sumerd"  value="{{  $data->{'Amount'}  }}">

                    <td class="">{{date('d-M-Y', strtotime($data->{'Posting Date'} ))}} </td>
                    <td class="">{{  $data->{'Description'}  }} </td>
                    <td class="bg-dark shadow-lg text-light" >{{ $b }}  </td>
                    <td class="">{{  $data->{'Payee'}  }} </td>
                    <td class="">{{  $data->{'Donor'}  }} </td>



                  </tr>

                    @endforeach
                    @endisset
                </tbody>

            </table>


</div>
</div>
</div>
