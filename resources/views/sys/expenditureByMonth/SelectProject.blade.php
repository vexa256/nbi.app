 <div class="col-12">
 <div class="card card-default">
          <div class="card-header">
          <h3 class="card-title">
        Select the project to attach the expenditure report to and click next

      <small class="font-weight-bold text-danger">

               Filter by month

            </small>
</h3>
            <i class="fas float-right fa-users fa-2x fa-fw"></i>

          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
               <form method="POST" action="{{ route('SelectProjectFilterByMonth') }}">

                <input type="hidden" name="Category" value="{{ $Category }}">
                <input type="hidden" name="Month" value="{{ $Month }}">
                 @csrf
                 <div class="row">
                <div class="col-md-12">
                 <div class="form-group">

                  <label> Select the project  to track</label>
                  <select name="Project" class="getdesc form-control select2bs4" style="width: 100%;">
                     <option selected="selected"></option>
                    @isset($Selections)
                        @foreach ($Selections as $data)


                    <option>{{ $data->GD1}}</option>

                        @endforeach
                    @endisset

                  </select>
                </div>
              </div>

              </div>

                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">

          <button  type="submit" class="btn btn-sm float-right btn-danger shadow-lg">

              <i class="fas fa-check "></i> next step

          </button>

  </form>


          </div>
        </div>
      </div>
