<!-- The Modal -->
<div class="modal" id="EmailRepor">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Email Report</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <form method="POST" action="{{ route('GenerateEmail') }}">
          @csrf

          <input type="hidden" name="Project" value="{{ $Project }}">
          <input type="hidden" name="Status" value="{{ $Stat }}">
          <input placeholder="Enter Recipient Email" required type="text" name="email" value="" class="form-control">



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger" >Send Mail</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
