
@isset($Contracts)
@foreach ($Contracts as $data)
<div class="modal" id="ViewMoesw33334{{  bin2hex($data->{'Id'}) }}">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">View More Information
        <small class="text-danger font-weight-bold">
        "{{ $Status }}"  tracking for the project {{ $Project }}
        </small>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>



                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="veiw2">{{ $data->{'No_'} }}</td>
                  <td class="veiw2">{{ number_format($data->{'Quantity'}, 2) }}</td>
                  <td class="veiw2">{{ number_format($data->{'Quantity Received'},2) }}</td>
                  <td class="veiw2">{{ date('d-M-Y', strtotime($data->{'Delivery Period'})) }}</td>
                  <td class="veiw2">{{ $data->{'Donor'} }}</td>
                  <td class="veiw2">{{ $data->{'Grant No'} }}</td>



                </tr>
              </tbody>
            </table>


          </div>
        </div>
         <div class="row">
          <div class="col-md-12">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="veiw2">Donor Output</th>
                  <th class="veiw2">Vendor N.O (Buy)</th>
                  <th class="veiw2">Vendor Name (Buy)</th>
                  <th class="veiw2">Contract Type</th>
                  <th class="veiw2">Start Date</th>


                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="veiw2">{{ $data->{'Donor Output'} }}</td>
                  <td class="veiw2">{{ stripslashes($data->{'Buy-from Vendor No_'}) }}</td>
                  <td class="veiw2">{{ $data->{'Buy-from Vendor Name'} }}</td>
                  <td class="veiw2">{{ $data->{'Contract Type'} }}</td>
                  <td class="veiw2">{{ $data->{'Contract Start Date'} }}</td>



                </tr>
              </tbody>
            </table>


          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-bordered table-striped">
              <thead>
           <tr>

                  <th class="view3">End Date</th>
                  <th class="view3">Currency Code</th>
                  <th class="view3">Amount (+ VAT)</th>
                  <th class="view3">Project</th>
                  <th class="view3">Activity</th>
                </tr>
              </thead>
              <tbody>
                <tr>

                  <td class="view3">{{ $data->{'Contract End Date'} }}</td>
                  <td class="view3">{{ $data->{'Currency Code'} }}</td>
                  <td class="view3">{{ number_format($data->{'Amount'},2) }}</td>
                  <td class="view3">{{ $data->{'GDC1'} }}</td>
                  <td class="view3">{{ $data->{'GDC2'} }}</td>

                </tr>
              </tbody>
            </table>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
@endisset
