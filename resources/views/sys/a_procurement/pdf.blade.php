<!DOCTYPE html>
<html>
<head>
  <title>{{ $Title }}</title>
</head>
<body>


        <h2>{{ $Title }}</h2>


      <table border="1" >
        <thead>
          <tr >
            <th >N.O</th>
            <th >Subject</th>
            <th >Category</th>
            <th >Status</th>
            <th >Description</th>
            <th >N.O</th>
            <th >Quantity</th>
            <th >Quantity Received</th>
            <th >Delivery Period</th>
            <th >Donor</th>
            <th >Grant N.O</th>
            <th >Donor Output</th>
            <th class="view2 ">Vendor N.O (Buy)</th>
            <th class="view2 ">Vendor Name (Buy)</th>
            <th >Contract Type</th>
            <th >Start Date</th>
            <th >End Date</th>
            <th >Currency Code</th>
            <th >Amount (+ VAT)</th>
            <th >Project</th>
            <th >Activity</th>
          </tr>
        </thead>
        <tbody>
          @isset($Contracts)
          @foreach ($Contracts as $data)
          <tr >
            <td >{{ $data->{'No_'} }}</td>
            <td >{{ $data->{'PD'} }}</td>
            <td >{{ $data->{'Category'} }}</td>
            <td >{{ $Status }}</td>
            <td >{{ $data->{'Description'} }}</td>
            <td >{{ $data->{'No_'} }}</td>
            <td >{{ number_format($data->{'Quantity'}, 2) }}</td>
            <td >{{ number_format($data->{'Quantity Received'},2) }}</td>
            <td >{{ date('d-M-Y', strtotime($data->{'Delivery Period'})) }}</td>
            <td >{{ $data->{'Donor'} }}</td>
            <td >{{ $data->{'Grant No'} }}</td>
            <td >{{ $data->{'Donor Output'} }}</td>
            <td >{{ stripslashes($data->{'Buy-from Vendor No_'}) }}</td>
            <td >{{ $data->{'Buy-from Vendor Name'} }}</td>
            <td >{{ $data->{'Contract Type'} }}</td>
            <td >{{ $data->{'Contract Start Date'} }}</td>
            <td >{{ $data->{'Contract End Date'} }}</td>
            <td >{{ $data->{'Currency Code'} }}</td>
            <td >{{ number_format($data->{'Amount'},2) }}</td>
            <td >{{ $data->{'GDC1'} }}</td>
            <td >{{ $data->{'GDC2'} }}</td>
          </td>
        </tr>


        @endforeach
        @endisset

</tbody>
</table>
</body>
</html>
