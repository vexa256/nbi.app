<div class="col-md-12">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title"> Contract Tracking Report For the Project
      <span class="text-danger font-weight-bold">{{ $Project }} </span>
      </h3>
      <span class="float-right text-danger font-weight-bold">
        {{ $Status }}  Filter
      </span>
    </div>

    <div class="alert alert">
      <div class="btn-group btn-group-toggle" data-toggle="buttons">
        <form method="POST" action="{{ route('GeneratePDF') }}">
          @csrf

          <input type="hidden" name="Project" value="{{ $Project }}">
          <input type="hidden" name="Status" value="{{ $Stat }}">

        <label class="btn btn-secondary active view_1">
          <input  type="submit" name="options" id="option_a1" autocomplete="off" checked=""> Generate PDF
        </label>
        </form>


      </div>
      <button data-target="#EmailRepor" class="btn btn-sm btn-danger float-right" data-toggle="modal">
        Send To Email
      </button>
    </div>


    <div class="card-body">


      <table style="width: 100% !important"  class="table data table-bordered table-primary table-responsive" >
        <thead>
          <tr >
            <th class="view ">N.O</th>
            <th class="view ">Subject</th>
            <th class="view">Category</th>
            <th class="view">Status</th>
            <th class="view">Description</th>
            <th class="view1">N.O</th>
            <th class="view1">Quantity</th>
            <th class="view1">Quantity Received</th>
            <th class="view1">Delivery Period</th>
            <th class="view1">Donor</th>
            <th class="view1">Grant N.O</th>
            <th class="view2">Donor Output</th>
            <th class="view2 ">Vendor N.O (Buy)</th>
            <th class="view2 ">Vendor Name (Buy)</th>
            <th class="view2">Contract Type</th>
            <th class="view2">Start Date</th>
            <th class="view3">End Date</th>
            <th class="view3">Currency Code</th>
            <th class="view3">Amount (+ VAT)</th>
            <th class="view3">Project</th>
            <th class="view3">Activity</th>
          </tr>
        </thead>
        <tbody>
          @isset($Contracts)
          @foreach ($Contracts as $data)
          <tr >
            <td class="view ">{{ $data->{'No_'} }}</td>
            <td class="view ">{{ $data->{'PD'} }}</td>
            <td class="view">{{ $data->{'Category'} }}</td>
            <td class="view">{{ $Status }}</td>
            <td class="view">{{ $data->{'Description'} }}</td>
            <td class="view1">{{ $data->{'No_'} }}</td>
            <td class="view1">{{ number_format($data->{'Quantity'}, 2) }}</td>
            <td class="view1">{{ number_format($data->{'Quantity Received'},2) }}</td>
            <td class="view1">{{ date('d-M-Y', strtotime($data->{'Delivery Period'})) }}</td>
            <td class="view1">{{ $data->{'Donor'} }}</td>
            <td class="view1">{{ $data->{'Grant No'} }}</td>
            <td class="view2">{{ $data->{'Donor Output'} }}</td>
            <td class="view2">{{ stripslashes($data->{'Buy-from Vendor No_'}) }}</td>
            <td class="view2">{{ $data->{'Buy-from Vendor Name'} }}</td>
            <td class="view2">{{ $data->{'Contract Type'} }}</td>
            <td class="view2">{{ $data->{'Contract Start Date'} }}</td>
            <td class="view3">{{ $data->{'Contract End Date'} }}</td>
            <td class="view3">{{ $data->{'Currency Code'} }}</td>
            <td class="view3">{{ number_format($data->{'Amount'},2) }}</td>
            <td class="view3">{{ $data->{'GDC1'} }}</td>
            <td class="view3">{{ $data->{'GDC2'} }}</td>
          </td>
        </tr>
        @endforeach
        @endisset
      </tbody>
    </table>
  </div>
</div>
</div>
@include('sys.a_procurement.email')
