 <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
     Nile Basin Initiative
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; NILE BASIN INITIATIVE  , All rights reserved.
  </footer>


<div class="modal" id="UpdatePasswords">
  <div class="modal-dialog">
    <div class="modal-content">


      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change Account Password</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      	<form action="{{ route('UpdateAccount') }}" method="POST">
      		@csrf
       <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label "> New Password</label>

                            <div class="col-md-6 mb-3">
                                <input id="password_new" type="password" class="form-control " name="password" value="" required >


                            </div>


                            <label for="password" class=" col-md-4 col-form-label "> Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password_conf" type="password" class="form-control "  value="" required >


                            </div>
                        </div>
                         <button type="button" class="btn float-left btn-primary" data-dismiss="modal">Close</button>
                         <button type="submit" class="btn float-right ckbd btn-danger">Save Changes</button>
                    </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

      </div>

    </div>
  </div>
</div>
