  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-12">
            <h5 class="m-0  alert-primary text-light alert shadow-lg">

                @isset($Title)

             <i class="fas fa-check-square  fa-fw">
            </i>
              {!!$Title!!}

                @endisset



            </h5>
          </div><!-- /.col -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
