<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NBI-IS ||   @isset($Title) {{$Title}} @endisset </title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ url('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/adminlte.min.css') }}">


  <link rel="stylesheet" type="text/css" href="{{ url('dt/data/datatables.min.css') }}"/>

  <link rel="stylesheet" href="{{ url('plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

    <link rel="stylesheet" href="{{ url('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">

  <style type="text/css">

  	table,td,th,tr,tbody,tfoot {

  		font-size: 14px !important;


  	}
    td{

        padding: 3px !important;
    }

 @if (Auth::user()->current_team_id != 'admin' ||
  Auth::user()->current_team_id != 'viewer' ||

  Auth::user()->current_team_id == '')

      .adminonly {

        display: none !important;

      }

    @endif


@if (Auth::user()->current_team_id == 'nbi user' ||

  Auth::user()->current_team_id == '')

  .hronly {

        display: none !important;

      }
      .admin-only {

        display: none !important;

      }

@endif


@if (Auth::user()->current_team_id == 'viewer')

      .viewer-only {

        display: none !important;

      }

@endif

@if (Auth::user()->current_team_id == 'supervisor')

      .supervisor-only {

        display: none !important;

      }

@endif



  </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
