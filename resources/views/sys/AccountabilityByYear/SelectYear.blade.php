 <div class="col-12">
 <div class="card card-dark shadow-lg ">
          <div class="card-header">
            <h3 class="card-title">Select the employee and year variables for the report to be generated


              <small class="text-danger font-weight-bold">Filtered By Year</small>


            </h3>

          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
               <form method="POST" action="{{ route('RunAccountabilityByYearAnalysis') }}">
                 @csrf
                 <div class="row">

                <div class="col-md-6">
                 <div class="form-group">

 @if (Auth::user()->current_team_id == 'nbi user' || Auth::user()->current_team_id == '' || Auth::user()->current_team_id == null)

                   <label>Select the Employee</label>
                  <select required  name="StaffCode" class="getdesc form-control select2bs4" style="width: 100%;">

                    <option value="{{ Auth::user()->EmployeeNo }}">

                      {{ Auth::user()->name }}

                    </option>

                  </select>
                  @endif

@if (Auth::user()->current_team_id == 'viewer')

                   <label>Select the Employee</label>
                  <select required  name="StaffCode" class="getdesc form-control select2bs4" style="width: 100%;">

                    <option value="{{ Auth::user()->EmployeeNo }}">

                      {{ Auth::user()->name }}

                    </option>

                  </select>
                  @endif



           @if ( Auth::user()->current_team_id == 'admin' ||   Auth::user()->current_team_id == 'supervisor')


                <label>Select the Employee</label>
                  <select required  name="StaffCode" class="getdesc form-control select2bs4" style="width: 100%;">
                     <option selected="selected"></option>
                    @isset($Users)
                        @foreach ($Users as $data)

                    <option value="{{ $data->StaffCode }}">{{ $data->Fname }}   {{ $data->Mname }}   {{ $data->Lname }}</option>

                        @endforeach
                    @endisset

                  </select>

                       @endif



                </div>
              </div>



              <div class="col-md-6">
                 <div class="form-group">

                <label>Select  Applicable Year</label>
                  <select required  name="Year" class="getdesc form-control select2bs4" style="width: 100%;">
                     <option selected="selected"></option>
                    @isset($Years)
                        @foreach ($Years as $data)


                    <option>{{ $data['PostingDate']}}</option>

                        @endforeach
                    @endisset

                  </select>



                </div>
              </div>


              </div>

                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">

          <button  type="submit" class="btn btn-sm float-right btn-danger shadow-lg">

              <i class="fas fa-check "></i> Generate Report

          </button>

  </form>


          </div>
        </div>
      </div>
