
<div class="col-md-12">

   <div class="card card-default shadow-lg">
          <div class="card-header">
            <h3 class="card-title p-2">Administrative Adavances  Analysis  Report Summary
            </h3>


            <table  class="table table-bordered  " >

                <thead>
                  <tr >
                    <th>Employee Name</th>
                    <th class="bg-dark text-light">Total Advanced Amount in USD</th>
                    <th class="bg-warning text-light">Total Accountabilities in USD</th>
                    <th class="bg-primary text-light">Accountability Variance in USD</th>
                    <th class="">Year</th>

                  </tr>
                </thead>

                <tbody>
                  <tr >
                    <td >{{ $EmployeeName }}</td>

                    <td class="bg-dark text-light">{{  number_format( $DebitAmount,2) }} USD</td>
                    <td class="bg-warning text-light">{{  number_format( $CreditAmount,2)}} USD</td>
                    <td class="bg-primary text-light">{{ number_format( $DebitAmount - $CreditAmount,2)}} USD</td>
                    <td class="">{{ $Year }}</td>


                  </tr>
                </tbody>

            </table>


</div>
</div>
</div>

























<div class="col-md-12">

   <div class="card card-default shadow-lg">
          <div class="card-header">
            <h3 class="card-title p-2">Detailed Administrative Adavances transaction logs

            </h3>
          </div>
          <div class="card-body">


            <table  class="data table table-bordered table-success table-responsive" >

                <thead>
                  <tr >
                    <th>Employee Name</th>
                    <th>Year</th>
                    <th> Advanced Amount</th>
                    <th>Accountability</th>

                    <th>Posting Date</th>
                    <th>Description</th>

                  </tr>
                </thead>

                <tbody>
                   @isset($Logs)
                     @foreach ($Logs as $data)
                  <tr >

                    <td>{{ $data->Fname }} {{ $data->Mname }} {{ $data->Lname }}</td>
                    <td>
                        <?php
$s = strtotime($data->PostingDate);

$Year = date('Y', $s);

echo $Year;?>



                    </td>
                    <td>{{  number_format($data->DebitAmount, 2)  }}</td>
                    <td>{{  number_format($data->CreditAmount, 2)  }}</td>
                    <td>{{ \Carbon\Carbon::parse($data->PostingDate)->format('j F, Y') }}</td>
                    <td>{{ $data->Description }}</td>




                  </tr>
                    @endforeach
                   @endisset
                </tbody>

            </table>


</div>
</div>
</div>
