@extends('beautymail::templates.widgets')

@section('content')

	@include('beautymail::templates.widgets.articleStart')

		<h4 class="secondary"><strong>Hello {{ $username }}</strong></h4>
		<p>
			This is to inform you that your leave application has been approved.

			<blockquote><strong>starting from the<span style="color: red"> {{date('d-M-Y', strtotime($from))}}</span>
                      to the <span style="color: red">{{date('d-M-Y', strtotime($to))}}</span>. The total number of leave days applied for and approved is

		<span style="color: red">{{ $days }}. Please contact the Human Resource Officer for the final instructions on how to proceed.</span></strong></blockquote>

			Kind regards.
		</p>

	@include('beautymail::templates.widgets.articleEnd')


	@include('beautymail::templates.widgets.newfeatureStart')

		<h4 class="secondary"><strong>About NBI</strong></h4>
		<p>

			<blockquote>
				<p>The Nile Basin Initiative (NBI) is an intergovernmental partnership of 10 Nile Basin countries, namely Burundi, DR Congo, Egypt, Ethiopia, Kenya, Rwanda, South Sudan, The Sudan, Tanzania and Uganda. Eritrea participates as an observer.
				</p>
			</blockquote>
			<blockquote>
							<p>
			For the first time in the Basin's history, an all-inclusive basin-wide institution was established, on 22nd February, 1999, to provide a forum for consultation and coordination among the Basin States for the sustainable management and development of the shared Nile Basin water and related resources for win-win benefits.</p>
						</blockquote>



						<blockquote>

		<p>

			for more information , please visit<strong> https://nilebasin.org/</strong>. This is a no-reply mail
		</p>
						</blockquote>

		</p>

	@include('beautymail::templates.widgets.newfeatureEnd')

@stop
