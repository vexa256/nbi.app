@include("sys.header.header")

@include("sys.nav.nav")


@include("sys.nav.sidebar")


<div class="content-wrapper">


	@include("sys.header.breadcrum")



	 <div class="content">
      <div class="container-fluid">
        <div class="row">

        	@isset($Page)
        	   @include($Page)
        	@endisset

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


</div>

@include("sys.footer.footer")
@include("sys.scripts.scripts")
