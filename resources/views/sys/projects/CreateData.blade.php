
            <!-- general form elements -->
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Create new "About Project" Profile</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" id="save-form" action="{{ route('CreateProjectData') }}">
                <div class="card-body  ">

                  @csrf
              <div class="form-group row">

                <div class="col-md-3">

                <label>Project Title</label>
                  <select required  name="title" class="getdesc form-control select2bs4" style="width: 100%;">
                     <option selected="selected"></option>
                    @isset($Projects)
                        @foreach ($Projects as $data)

                    <option >{{ $data->Project }} </option>

                        @endforeach
                    @endisset

                  </select>
                </div>


              <div class="col-md-3">
                <label>Start Date</label>
                <div class="input-group date" id="start_date" data-target-input="nearest">
                  <input required type="text" name="start_date" class="form-control datetimepicker-input" data-target="#start_date">
                  <div class="input-group-append" data-target="#start_date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
              </div>



              <div class="col-md-3">
                <label>End Date</label>
                <div class="input-group date" id="end_date" data-target-input="nearest">
                  <input required type="text" name="end_date" class="form-control datetimepicker-input" data-target="#end_date">
                  <div class="input-group-append" data-target="#end_date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
              </div>


              <div class="col-md-3">
                <label>Budget</label>

                  <input required type="number" name="budget" class="form-control">




              </div>
            </div>

                  <div class="form-group">
                    <label >Introduction</label>
                  <textarea required name="introduction" class="app_letters"></textarea>
                  </div>

                   <div class="form-group">
                    <label >Budget Holder</label>
                  <textarea required name="Budget_Holder" class="app_letters"></textarea>
                  </div>


                  <div class="form-group">
                    <label >Objectives</label>
                  <textarea required name="hypotheses_objectives" class="app_letters"></textarea>
                  </div>



                    <div class="form-group">
                    <label >Participants, organization and collaborations</label>
                  <textarea required name="partners" class="app_letters"></textarea>
                  </div>



                </div>
                <!-- /.card-body -->

                <div class="card-footer">

                </div>

            </div>
