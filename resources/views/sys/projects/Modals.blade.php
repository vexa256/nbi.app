 @isset($ProjectData)
               @foreach ($ProjectData as $data)

               <div class="modal" id="hypotheses_objectives{{ $data->id }}">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Project   Objectives </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" >


          <textarea class="app_letters">

              {{ $data->hypotheses_objectives }}

          </textarea>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </form>
      </div>

    </div>
  </div>
</div>
@endforeach
            @endisset




@isset($ProjectData)
               @foreach ($ProjectData as $data)

               <div class="modal" id="introduction{{ $data->id }}">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Project Description</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" >


          <textarea class="app_letters">

              {{ $data->introduction }}

          </textarea>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </form>
      </div>

    </div>
  </div>
</div>
@endforeach
            @endisset






@isset($ProjectData)
               @foreach ($ProjectData as $data)

               <div class="modal" id="partners{{ $data->id }}">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Project Partners</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" >


          <textarea class="app_letters">

              {{ $data->partners }}

          </textarea>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </form>
      </div>

    </div>
  </div>
</div>
@endforeach
            @endisset



            @isset($ProjectData)
               @foreach ($ProjectData as $data)

               <div class="modal" id="Budget_Holder{{ $data->id }}">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Project Budget Holder</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" >


          <textarea class="app_letters">

              {{ $data->Budget_Holder }}

          </textarea>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </form>
      </div>

    </div>
  </div>
</div>
@endforeach
            @endisset
