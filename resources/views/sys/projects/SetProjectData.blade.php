<div class="col-md-12">

	 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Manage project summary and description data</h3>

            <a href="#CreateNewData" data-toggle="modal" class="float-right btn btn-danger btn-sm shadow-lg">

              <i class="fas fa-plus"></i> New Profile

            </a>
      </div>
      <div class="card-body">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <table  class="table data table-bordered table-primary " >

            		<thead>
            			<tr >
            				<th>Title</th>
                    <th>Duration</th>

                      <th class="">Budget (USD)</th>
                     <th class="">Objectives</th>
                    <th>Description</th>
                    <th>Partners</th>

                    <th class="">Budget Holder</th>







            			</tr>
            		</thead>

	<tbody>
            @isset($ProjectData)
               @foreach ($ProjectData as $data)
              <tr >
                  <td>{{ $data->title }}</td>
                  <td>{{date('d-M-Y', strtotime($data->start_date))}} to {{date('d-M-Y', strtotime($data->end_date))}}</td>

                  <td>{{ number_format($data->budget, 2) }} </td>
                  <td> <a href="#hypotheses_objectives{{ $data->id }}" class="btn btn-sm btn-danger" data-toggle="modal"> <i class="fas fa-binoculars"></i> </a> </td>
                  <td> <a href="#introduction{{ $data->id }}" class="btn btn-sm btn-danger" data-toggle="modal"> <i class="fas fa-binoculars"></i> </a> </td>
                  <td> <a href="#partners{{ $data->id }}" class="btn btn-sm btn-danger" data-toggle="modal"> <i class="fas fa-binoculars"></i> </a> </td>
                  <td> <a href="#Budget_Holder{{ $data->id }}" class="btn btn-sm btn-danger" data-toggle="modal"> <i class="fas fa-binoculars"></i> </a> </td>


            </tr>
            @endforeach
            @endisset


	</tbody>

            </table>


</div>
</div>
</div>

@include('sys.donors.CreateInvoice')
@include('sys.projects.CreateDataModal')
@include('sys.projects.Modals')
