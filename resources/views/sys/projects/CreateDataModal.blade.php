<div class="modal" id="CreateNewData">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Project Data Settings </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll">
      @include('sys.projects.CreateData')
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button  type="submit" class="btn btn-primary">Save Changes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </form>
      </div>

    </div>
  </div>
</div>
