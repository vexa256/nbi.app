<!-- Main Sidebar Container -->
<aside style="background-color: #000063 !important" class="text-light main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <i class="fas fa-project-diagram fa-2x text-light " style="opacity: .8"></i>
    <span class="brand-text font-weight-light">NBI-IS</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <i class="fas fa-user fa-2x text-light " style="opacity: .8"></i>
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
      </div>
    </div>
    <!-- SidebarSearch Form -->
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
        with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview admin-only viewer-only">
          <a href="#" class="nav-link ">
            <i class="nav-icon fas fa-chart-pie  fa-2x fa-fw"></i>
            <p>
              Variance Report
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item admin-only viewer-only">
              <a href="{{ route('VarianceReportSelectProject') }}" class="nav-link ">
                <i class="fa-fw fas fa-chart-line nav-icon"></i>
                <p>Generate Report</p>
              </a>
            </li>

          </ul>
        </li>
         <li class="nav-item has-treeview admin-only viewer-only">
          <a href="#" class="nav-link ">
            <i class="nav-icon fas fa-money-check-alt  fa-2x fa-fw"></i>
            <p>
              Procurement
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item admin-only viewer-only">
              <a href="{{ route('ProcurementSelectProject', ['Status' => '0']) }}" class="nav-link ">
                <i class="fa-fw fas fa-book-open nav-icon"></i>
                <p>Open Contracts</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('ProcurementSelectProject', ['Status' => '1']) }}" class="nav-link ">
                <i class="fas fa-check nav-icon"></i>
                <p>Released Contracts</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('ProcurementSelectProject', ['Status' => '2']) }}" class="nav-link ">
                <i class="fas fa-clock nav-icon"></i>
                <p>Pending Contracts</p>
              </a>
            </li>
          </ul>
        </li>
<li class="nav-item has-treeview supervisor-only admin-only">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-project-diagram  fa-2x fa-fw"></i>
              <p>
              Project Data


              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item admin-only viewer-only">
                <a href="{{ route('SetProjectData') }}" class="nav-link ">
                  <i class="fa-fw fas fa-wrench nav-icon"></i>
                  <p>Set Project Data</p>
                </a>
              </li>

               <li class="nav-item">
                <a href="{{ route('SelectProjectToViewSummary') }}" class="nav-link ">
                  <i class="fas fa-binoculars nav-icon"></i>
                  <p>Project Summary</p>
                </a>
              </li>

            </ul>
          </li>




      <li class="nav-item has-treeview ">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-comment-dollar fa-2x fa-fw"></i>
              <p>
              Finance


              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item admin-only admin-only ">
                <a href="{{ route('DonorSelectYear') }}" class="nav-link ">
                  <i class="fa-fw fas fa-funnel-dollar nav-icon"></i>
                  <p>Donor Funding Analysis </p>
                </a>
              </li>

               <li class="nav-item admin-only supervisor-only">
                <a href="{{ route('DonoroAnalyticsSelectYear') }}" class="nav-link ">
                  <i class="fas fa-dollar-sign nav-icon"></i>
                  <p>Invoiced VS Paid</p>
                </a>
              </li>

               <li class="nav-item viewer-only admin-only supervisor-only">
                <a href="{{ route('SetBenchMark') }}" class="nav-link ">
                  <i class="fas fa-cogs nav-icon"></i>
                  <p>Set Benchmarks</p>
                </a>
              </li>


              <li class="nav-item admin-only supervisor-only">
                <a href="{{ route('SelectExpenditureCategory') }}" class="nav-link ">
                  <i class="fa-fw fas fa-calendar-plus nav-icon"></i>
                  <p>Expenditure (Year)</p>
                </a>
              </li>

              <li class="nav-item admin-only supervisor-only">
                <a href="{{ route('FilterByMonthCategory') }}" class="nav-link ">
                  <i class="fa-fw fas fa-calendar-minus nav-icon"></i>
                  <p>Expenditure (Month)</p>
                </a>
              </li>

               <li class="nav-item ">
                <a href="{{ route('AdminAdvancesSelectYear') }}" class="nav-link ">
                  <i class="fa-fw fas fa-calendar-check nav-icon"></i>
                  <p>Advances (Year)</p>
                </a>
              </li>

               <li class="nav-item">
                <a href="{{ route('SelectAdminAdvancesMonth') }}" class="nav-link ">
                  <i class="fas fa-calendar-week nav-icon"></i>
                  <p>Advances (Month)</p>
                </a>
              </li>

               <li class="nav-item ">
                <a href="{{ route('AccountabilityByYear') }}" class="nav-link ">
                  <i class="fa-fw fas fa-calendar-check nav-icon"></i>
                  <p>Accountability  (Year)</p>
                </a>
              </li>

               <li class="nav-item">
                <a href="{{ route('AccountabilityReportByMonth') }}" class="nav-link ">
                  <i class="fas fa-calendar-week nav-icon"></i>
                  <p>Accountability (Month)</p>
                </a>
              </li>


            </ul>
          </li>




        <li class="nav-item has-treeview ">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-user-alt-slash  fa-2x fa-fw"></i>
              <p>
                Human Resource


              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item admin-only">
                <a href="{{ route('ApproveLeave') }}" class="nav-link ">
                  <i class="fa-fw fas fa-check nav-icon"></i>
                  <p>Approve Leave</p>
                </a>
              </li>

               <li class="nav-item">
                <a href="{{ route('ApplyForLeave') }}" class="nav-link ">
                  <i class="fas fa-book nav-icon"></i>
                  <p>Apply for leave</p>
                </a>
              </li>




               <li class="nav-item admin-only ">
                <a href="{{ route('ManageSupervisors') }}" class="nav-link ">
                  <i class="fa-fw fas fa-wrench nav-icon"></i>
                  <p>Manage Supervisors</p>
                </a>
              </li>



               <li class="nav-item admin-only  viewer-only">
                <a href="{{ route('AssignSupervisor') }}" class="nav-link ">
                  <i class="fas fa-chart-pie nav-icon"></i>
                  <p>Assign  Supervisor</p>
                </a>
              </li>







            </ul>
          </li>

 <li class="nav-item has-treeview admin-only supervisor-only">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-users-cog fa-2x fa-fw"></i>
              <p>
              Settings


              </p>
            </a>
            <ul class="nav nav-treeview">


              <li class="nav-item">
                <a href="{{ route('ManageUsersGeneric') }}" class="nav-link ">
                  <i class="fas fa-user-shield nav-icon"></i>
                  <p>Manage Users</p>
                </a>
              </li>

               <li class="nav-item">
                <a href="{{ route('ViewAllUsers') }}" class="nav-link ">
                  <i class="fas fa-users nav-icon"></i>
                  <p>All Users</p>
                </a>
              </li>

                <li class="nav-item">
                <a href="{{ route('SetHRmail') }}" class="nav-link ">
                  <i class="fas fa-cogs nav-icon"></i>
                  <p>Set HR mail</p>
                </a>
              </li>


               <li class="nav-item">
                <a href="{{ route('DumpCache') }}" class="nav-link ">
                  <i class="fas fa-wrench nav-icon"></i>
                  <p>Force Cache Dump</p>
                </a>
              </li>





            </ul>
          </li>



















        <li class="nav-item">
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>
              Logout
            </p>
          </a>
        </li>































































      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  {{ csrf_field() }}
</form>
