<!-- Navbar -->
  <nav class=" main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>

    </ul>



    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link"  href="{{ url('/') }}">
     <i class="fas fa-home fa-2x text-secondary"></i>

        </a>

      </li>

@if (Auth::user()->role == 'adminsssssssssssssssss')
   <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
     <i class="fas fa-money-bill fa-2x text-warning"></i>

        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">Accountability Reports</span>
          <div class="dropdown-divider"></div>
          <a href="" class="dropdown-item">
            <i class="fas fa-binoculars mr-2"></i>View Report
            <span class="float-right text-muted text-sm"></span>
          </a>




        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
   <i class="fas fa-donate fa-2x text-primary"></i>

        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">Donor Reports</span>
          <div class="dropdown-divider"></div>
          <a href="" class="dropdown-item">
            <i class="fas fa-binoculars mr-2"></i>View Report
            <span class="float-right text-muted text-sm"></span>
          </a>




        </div>
      </li>


        <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
 <i class="fas fa-comments-dollar fa-2x text-danger"></i>

        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">Project Activity Reports</span>
          <div class="dropdown-divider"></div>
          <a href="" class="dropdown-item">
            <i class="fas fa-binoculars mr-2"></i>View Report
            <span class="float-right text-muted text-sm"></span>
          </a>




        </div>
      </li>

@endif


      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fas fa-users fa-2x text-success"></i>

        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">Account Settings</span>
          <div class="dropdown-divider"></div>
          <a href="#UpdatePasswords" data-toggle="modal" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i>My Account
            <span class="float-right text-muted text-sm"></span>
          </a>

          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i>Logout
            <span class="float-right text-muted text-sm"></span>
          </a>


        </div>
      </li>

    </ul>
  </nav>
  <!-- /.navbar -->
