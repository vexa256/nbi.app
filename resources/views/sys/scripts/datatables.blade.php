<script type="text/javascript">
$(function () {

    $('.data').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      dom: 'Bfrtip',
        buttons: [



            {
                extend: 'excelHtml5',

            },

            'colvis'
        ]
    });


    $('.filter_a_a').DataTable({
      "paging": true,
       "pageLength" : 5,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
