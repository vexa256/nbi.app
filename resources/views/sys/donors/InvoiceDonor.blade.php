<div class="col-md-12">

	 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Manage Donor Contributions Benchmarking</h3>

            <a href="#myModalssssssss" data-toggle="modal" class="btn float-right btn-dark btn-sm shadow-lg">
                  <i class="fas fa-user-plus"></i> New Benchmark
            </a>
      </div>
      <div class="card-body">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <table  class="table data table-bordered table-primary " >

            		<thead>
            			<tr >
            				<th>Donor</th>
                    <th>Year</th>
            				<th class="bg-dark shadow-lg text-light">Invoiced Amount</th>
                    <th class="d-none bg-dark shadow-lg text-light">Current Contribution</th>
                    <th class="d-none bg-dark shadow-lg text-light">Outstanding Balance</th>
            				<th>Delete Entry</th>


            			</tr>
            		</thead>

	<tbody>
            @isset($Donors)
               @foreach ($Donors as $data)
              <tr >
                  <td >{{ $data->Donor }}</td>
                  <td >{{ $data->Year }}</td>
                  <td >{{ number_format($data->InvoicedAmount )}} USD</td>
                  <td class="d-none">{{ number_format($data->CurrentAmount )}} USD</td>
                  <td  class="d-none">{{ number_format($data->Outstanding )}} USD</td>
                  <td >
                        <a href="{{ route('DeleteBenchMark', ['id' => $data->id]) }}" class="btn btn-sm btn-danger shadow-lg">
                              <i class="fas fa-trash"></i>
                              Delete
                        </a>

                  </td>
            </tr>
            @endforeach
            @endisset


	</tbody>

            </table>


</div>
</div>
</div>

@include('sys.donors.CreateInvoice')
