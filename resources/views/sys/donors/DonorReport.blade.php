  <div class="col-md-12">
            <!-- small box -->
            <div class="small-box shadow-lg bg-dark">
              <div class="inner">
                <h3>USD {{ number_format($ac) }}</h3>

                <p>Total donor ({{ $DonorsName }}) funding for the year {{ $DonorYear }}</p>
              </div>
              <div class="icon">
                <i class="fas text-light fa-lock"></i>
              </div>

            </div>
          </div><div class="col-md-6">
 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">
            	<span class="btn bg-dark shadow-lg text-light">
            		{{ $DonorsName }}
            	</span>

            	donor funding report for the year
            	<span class="btn bg-danger shadow-lg text-light">

            		{{ $DonorYear }}
            	</span>

            </h3>

          </div>
          <!-- /.card-header -->
          <div class="card-body" style="max-height: 250px">

          				{!! $chart->container() !!}

          </div>
      </div>
  </div>


  <div class="col-md-6">

	 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Donor <span class="btn bg-danger shadow-lg">

            	({{ $DonorsName }})
            </span> funding logs for the year <span class="btn bg-dark shadow-lg">

            	{{ $DonorYear }}

            </span></h3>

</div>
</div>
<div class="card-body">
            <table  class="table data table-bordered table-primary table-responsive" >

            		<thead>
            			<tr >
            				<th >Donor</th>
            				<th>GL Account</th>
            				<th>Description</th>
            				<th class=" bg-danger shadow-lg">Amount</th>
            				<th>Posting Date</th>


            			</tr>
            		</thead>

            		<tbody>

            			@isset($donors)
            			  @foreach ($donors as $data)
            			 <tr >
            				<td >{{ $data->Donor }}</td>
            				<td >{{ $data->G_L_AccountNo }}</td>
            				<td >{{ $data->Description }}</td>
            				<td class=" bg-danger shadow-lg" >USD {{ $data->Amount }}</td>
            				<td >{{ $data->PostingDate }}</td>

            			</tr>
            			  @endforeach
            			@endisset

            		</tbody>

            </table>


</div>
</div>
