

    <div class="col-md-12">

	 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title"> Donor Invoiced and Contributed Amount Report</h3>


      </div>
      <div class="card-body">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <table  class="table data table-bordered " >

            		<thead>
            			<tr >
            				<th>Donor</th>
                    <th>Year</th>
            				<th class="bg-danger shadow-lg text-light">Invoiced Amount</th>
                    <th class="bg-dark shadow-lg text-light">Current Contribution</th>
                    <th class="bg-primary shadow-lg text-light">Outstanding Balance</th>



            			</tr>
            		</thead>

	<tbody>
            @isset($Donors)
               @foreach ($Donors as $data)
              <tr >
                  <td >{{ $data->Donor }}</td>
                  <td >{{ $data->Year }}</td>
                  <td class="InvoicedAmount" data-amount="{{ $data->InvoicedAmount }}">{{ number_format($data->InvoicedAmount )}} USD</td>
                  <td class="CurrentAmount" data-amount="{{ $data->CurrentAmount }}">{{ number_format($data->CurrentAmount )}} USD</td>
                  <td class="Outstanding" data-amount="{{ $data->Outstanding }}">{{ number_format($data->Outstanding )}} USD</td>

                  </td>
            </tr>
            @endforeach
            @endisset


	</tbody>

            </table>


</div>
</div>
</div>
