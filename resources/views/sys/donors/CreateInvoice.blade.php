<!-- The Modal -->
<div class="modal" id="myModalssssssss">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Set new donor contribution benchmark</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body ">
         <form method="POST" action="{{ route('SetNewBenchMark') }}" class="row">

            @csrf
        <div class="col-md-4">
              <div class="form-group">
                <label> Select Donor </label>
                <select required name ="Donor" class="getdesc form-control select2bs4">
                  <option selected="selected"></option>
                  @isset($DonorReports)
                  @foreach ($DonorReports as $data)
                  <option >{{ $data->Donor }}</option>
                  @endforeach
                  @endisset
                </select>
              </div>
            </div>


              <div class="col-md-4">
              <div class="form-group">
                <label> Amount </label>
                <input type="number" name="InvoicedAmount" value="" class="form-control" required>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group">
                <label> Select Year </label>
                <select required name ="Year" class="getdesc form-control select2bs4">
                  <option selected="selected"></option>
                  @isset($Years)
                  @foreach ($Years as $data)
                  <option >{{ $data->Year }}</option>
                  @endforeach
                  @endisset
                </select>
              </div>
            </div>
              <button type="submit" class="mr-3 btn btn-danger" >Save</button>

              <button type="button" class="btn btn-dark ml-3" data-dismiss="modal">Close</button>
            </form>
          </div>



    </div>
  </div>
</div>
