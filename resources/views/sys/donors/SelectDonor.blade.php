 <div class="col-12">
 <div class="card card-default">
          <div class="card-header">
          <h3 class="card-title">
            Hello, {{ Auth::user()->name  }}, Please select the the donor in relation to the donor report filtering</h3>
            <i class="fas float-right fa-users fa-2x fa-fw"></i>

          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
               <form method="POST" action="{{ route('GenerateDonorReport') }}">
                 @csrf
                 <div class="row">
                <div class="col-md-12">
                 <div class="form-group">

                  <label> Select Donor's Name</label>
                  <input type="hidden" name="DonorYear" value="{{ $DonorYear }}">
                  <select name="DonorsName" class="getdesc form-control select2bs4" style="width: 100%;">
                     <option selected="selected"></option>
                    @isset($Donors)
                        @foreach ($Donors as $data)


                    <option>{{ $data->Donor }}</option>

                        @endforeach
                    @endisset

                  </select>
                </div>
              </div>

              </div>

                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">

          <button  type="submit" class="btn btn-sm float-right btn-danger shadow-lg">

              <i class="fas fa-check "></i> Generate Report

          </button>

  </form>


          </div>
        </div>
      </div>
