<div class="modal" tabindex="-1" role="dialog" id="MyLeaveApps">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">All your leave applications
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="max-height: 400px ; overflow-y: scroll">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              <h3 class="card-title">This is a detailed log of all your leave applications</h3>
            </div>
            <div class="card-body">
              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
              <table style=""  class="table  table-bordered table-striped " >
                <thead>
                  <tr >
                    <th class=" ">Employee NO</th>
                    <th class=" ">From Date</th>
                    <th class="">To Date</th>
                    <th class=" ">Total Leave Days</th>
                    <th class=" bg-danger shadow-lg text-light">Status</th>
                    <th class=" bg-primary shadow-lg text-light">Application Letter</th>
                    <th class=" bg-dark shadow-lg text-light">Cancel Applications</th>
                  </tr>
                </thead>
                <tbody>
                  @isset($LeaveApplications)
                  @foreach ($LeaveApplications as $data)
                  <tr >
                    <td>{{$data->U_EmployeeNo}}</td>
                    <td>{{date('d-M-Y', strtotime($data->from_date))}} </td>
                     <td>{{date('d-M-Y', strtotime($data->to_date))}} </td>
                    <td>{{$data->days}}</td>
                    <td class="bg-dark shadow-lg text-light">{{$data->status}}</td>
                    <td>
                      <a data-dismiss="modal" href="#LeaveApplications{{$data->UNI}}" data-toggle="modal" class="btn btn-sm btn-danger shadow-lg">
                        <i class="fas fa-binoculars"></i>
                      View </a>
                    </td>
                    <td>
                      @if ($data->status == 'pending')
                        <a href="{{ route('TerminateLeaveApp', ['id' => $data->id]) }}" class="btn btn-sm btn-danger shadow-lg text-light">
                        <i class="fas fa-trash"></i>
                      </a>
                      @endif

                       @if ($data->status != 'pending')

                          Applicable only if status is "pending"

                      @endif

                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
