<div class="modal" id="assignsupervisor">
  <div class="modal-dialog ">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h6 class="modal-title">Assign the employee

          <span class="font-weight-bold text-danger dataname"></span>

          supervisor(s)
        </h6>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form method="POST" action="{{ route('AssignSupervisorLogic') }}">
          @csrf

          <input required type="hidden" class="user_id" name="user" value="" >

           <div class="col-md-12">
              <div class="form-group">
                <label> Select supervisor </label>
                <select required name ="supervisor" class="getdesc form-control select2bs4">
                  <option selected="selected"></option>
                  @isset($Eleveted)
                  @foreach ($Eleveted as $data)
                  <option value="{{$data->UNI}}">{{ $data->name }}</option>
                  @endforeach
                  @endisset
                </select>
              </div>
            </div>

           <button type="submit" class="btn btn-primary shadow-lg">Save Changes</button>

            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>


        </form>
      </div>

      <!-- Modal footer -->


    </div>
  </div>
</div>
