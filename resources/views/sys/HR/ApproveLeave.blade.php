  <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              <h3 class="card-title">Hello , {{ Auth::user()->name }}, Use this interface to manage leave applications of employees you supervise.
             </h3>
             <a href="#Approved_Applications" data-toggle="modal" class=" float-right btn btn-sm btn-danger shadow-lg">
               Approved
             </a>

              <a href="#DeclinedApplications" data-toggle="modal" class="mr-2 float-right btn btn-sm btn-danger shadow-lg">
               Declined
             </a>
            </div>
            <div class="card-body">
                <div class="callout callout-danger shadow-lg">
          <h5 class="text-danger"></h5>
          <p>
         A leave application is only completely approved after  all supervisors assigned to the applicant have approved it, partially approved applications have a status of pending. One supervisor declining the leave application terminates it</p>




        </div>
              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Pending Leave Applications</h3>
                </div>
              </div>
              <table style=""  class="table data table-bordered table-striped " >
                <thead>
                  <tr >
                    <th class=" ">Employee Name</th>
                    <th class=" ">Employee NO</th>
                    <th class=" ">From Date</th>
                    <th class="">To Date</th>
                    <th class=" ">Total Leave Days</th>
                    <th class=" bg-danger shadow-lg text-light">Status</th>
                    <th class=" ">Application Letter</th>
                    <th class=" ">Approve</th>
                    <th class=" ">Decline</th>
                  </tr>
                </thead>
                <tbody>
                  @isset($LeaveApplications)
                  @foreach ($LeaveApplications as $data)
                  <tr >
                    <td>{{$data->Name}}</td>
                    <td>{{$data->U_EmployeeNo}}</td>
                    <td>{{date('d-M-Y', strtotime($data->from_date))}} </td>
                     <td>{{date('d-M-Y', strtotime($data->to_date))}} </td>
                    <td>{{$data->days}}</td>
                    <td class="bg-danger shadow-lg text-light">{{$data->status}}</td>
                    <td>
                        <a data-toggle="modal" href="#LeaveApplications{{$data->UNI}}" class="btn btn-danger btn-sm shadow-lg">

                          <i class="fas fa-binoculars"></i> View

                        </a>

                    </td>

                    <td>
                        <a href="{{ route('ApproveLeaveLogic', ['id' => $data->UNI]) }}" class="btn btn-danger btn-sm shadow-lg">

                          <i class="fas fa-check"></i> Approve

                        </a>

                    </td>


                     <td>
                        <a href="{{ route('DeclineLeaveLogic', ['id' => $data->UNI]) }}" class="btn btn-danger btn-sm shadow-lg">

                          <i class="fas fa-times"></i> Decline

                        </a>

                    </td>

                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>


        @include('sys.HR.ViewAppLetter')
        @include('sys.HR.ApprovedApps')
        @include('sys.HR.DeclinedApps')
