<div class="col-md-3">
  <div class="info-box bg-info shadow-lg">
    <span class="info-box-icon"><i class="fas fa-calculator"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Available Annual Leave Days</span>
      <span class="info-box-number">{{Auth::user()->LeaveDays}}</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<div class="col-md-3">
  <div class="info-box bg-primary shadow-lg">
    <span class="info-box-icon"><i class="fas fa-users"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Total Assigned Supervisors
        <span class="info-box-number">{{$SupervisorsCount}}</span>

      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <div class="col-md-3">
    <div class="info-box bg-dark shadow-lg">
      <span class="info-box-icon"><i class="fas fa-check"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Aproved Applications
          <span class="info-box-number">{{ $Approved }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3">
      <div class="info-box bg-danger shadow-lg">
        <span class="info-box-icon"><i class="fas fa-spinner"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Pending Applications
            <span class="info-box-number">{{ $Pending }}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <div class="col-md-12">
        @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
        <div class="callout callout-info">
          <h5 class="text-danger">Hello {{Auth::user()->name}}</h5>
          <p>
          Use this interface to apply for leave. Your available leave days will only be updated after your application is approved by your all supervisors</p>

          <button data-toggle="modal" data-target="#applications_AAAA" class="btn text-light btn-sm shadow-lg btn-danger">

            <i class="fas fa-plus"></i> Apply

          </button>

          <button data-target="#MyLeaveApps" data-toggle="modal" class="text-light btn btn-sm shadow-lg btn-danger">

            <i class="fas fa-cogs"></i> View Applications

          </button>



          <button data-target="#Mysupervisors" data-toggle="modal" class="text-light btn btn-sm shadow-lg btn-dark">

            <i class="fas fa-binoculars"></i>Supervisors

          </button>



        </div>
      </div>

@include('sys.HR.MyLeaveApps')
@include('sys.HR.ApplyModal')
@include('sys.HR.MySupervisors')
@include('sys.HR.ViewAppLetter')
