<div class="col-md-12">

	 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Use this interface to assign human resource supervisors to employees </h3>



      </div>
      <div class="card-body">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <table  class="table filter_a_a table-bordered table-striped " >

            		<thead>
            			<tr >
            				<th class=" bg-primary shadow-lg text-light">Name</th>
                    <th class=" bg-primary shadow-lg text-light">Job Title</th>
            				<th>Employee Code</th>
                    <th>Assign Supervisors</th>
                    <th>View Supervisors</th>


            			</tr>
            		</thead>

	<tbody>
            @isset($Supervisors)
               @foreach ($Supervisors as $data)
              <tr >
                  <td class=" bg-primary shadow-lg text-light">{{ $data->name }}</td>
                  <td class=" bg-primary shadow-lg text-light">{{ $data->JobTitle }}</td>
                  <td >{{ $data->EmployeeNo }}</td>

                  <td >
                        <a data-id="{{$data->UNI}}" data-name="{{ $data->name }}" href="#" class=" avhsvshsa assign btn btn-sm btn-primary text-light shadow-lg">
                              <i class="fas fa-plus"></i>
                              Assign
                        </a>

                  </td>




                  <td >
                    <form method="POST" action="{{ route('ViewAssignedSupervisors') }}">
                      @csrf
                      <input type="hidden" name="UserEmpNo" value="{{ $data->EmployeeNo }}" >

                        <button type="submit" class=" avhsvshsa btn btn-sm btn-danger text-light shadow-lg">
                              <i class="fas fa-binoculars"></i>
                              View
                        </button>

                           </form>

                  </td>
            </tr>
            @endforeach
            @endisset


	</tbody>

            </table>


</div>
</div>
</div>
