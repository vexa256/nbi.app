<div class="modal"  id="Approved_Applications">
  <div class="modal-dialog modal-dialog modal-xl" >
    <div class="modal-content">
      <div class="modal-header bg-primary text-light">
        <h5 class="modal-title text-light">Leave appplications you approved</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >

        <table style=""  class="table data table-bordered table-striped " >
                <thead>
                  <tr >
                    <th class=" ">Employee Name</th>
                    <th class=" ">Employee NO</th>
                    <th class=" ">From Date</th>
                    <th class="">To Date</th>
                    <th class=" ">Total Leave Days</th>
                    <th class=" bg-danger shadow-lg text-light">Status</th>
                    <th class=" ">Application Letter</th>

                  </tr>
                </thead>
                <tbody>
                  @isset($ApprovedApplications)
                  @foreach ($ApprovedApplications as $data)
                  <tr >
                    <td>{{$data->Name}}</td>
                    <td>{{$data->U_EmployeeNo}}</td>
                    <td>{{date('d-M-Y', strtotime($data->from_date))}} </td>
                     <td>{{date('d-M-Y', strtotime($data->to_date))}} </td>
                    <td>{{$data->days}}</td>
                    <td class="bg-danger shadow-lg text-light">{{$data->status}}</td>
                    <td>
                        <a data-dismiss="modal" data-toggle="modal" href="#LeaveApplicationsz{{$data->UNI}}" class="btn btn-danger btn-sm shadow-lg">

                          <i class="fas fa-binoculars"></i> View

                        </a>

                    </td>


                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           </div>
         </div>
       </div>
     </div>
