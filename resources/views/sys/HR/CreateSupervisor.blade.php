<div class="modal" id="CreateSupervisor">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Create New Supervisor</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" >
        <form method="POST" action="{{ route('CreateSupervisor') }}">
          @csrf
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Supervisor Role/Title</label>
                <input required class="form-control" type="text" name="role" value="" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label> Select user to assign role</label>
                <select required name ="user" class="getdesc form-control select2bs4">
                  <option selected="selected"></option>
                  @isset($Users)
                  @foreach ($Users as $data)
                  <option value="{{$data->id}}">{{ $data->name }}</option>
                  @endforeach
                  @endisset
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Supervisor Role Description</label>
                <textarea name="desc" class="app_letters"></textarea>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-danger" >Save Changes</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
