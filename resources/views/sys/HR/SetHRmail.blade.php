<div class="col-md-12">

	 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Use this interface to set the Human Resource Office Email </h3>

            <small class="ml-3 text-danger font-weight-bold"> Recieves employee leave  application forms (Critical)</small>

      </div>
      <div class="card-body">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <table  class="table filter_a_a table-bordered table-striped " >

            		<thead>
            			<tr >
            				<th class=" bg-primary shadow-lg text-light">Email</th>
                    <th class=" bg-primary shadow-lg text-light">Update</th>



            			</tr>
            		</thead>

	<tbody>
              <tr >
                  <td class=" bg-primary shadow-lg text-light">{{ $data->mail }}</td>
                  <td >

                    <a data-toggle="modal" href="#myMddddodal" class="btn btn-danger shadow-lg">Update Email</a>

                  </td>

            </tr>



	</tbody>

            </table>


</div>
</div>
</div>



<div class="modal" id="myMddddodal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Update Human Resource Office Mail</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

        <form action="{{ route('CreateHRMail') }}" method="POST">
         @csrf

          <div class="row">
          <div class="col-md-12">
          <div class="form-group">

            <input type="text" class="form-control" name="mail" value="{{ $data->mail }}">
            <input type="hidden" class="form-control" name="id" value="{{ $data->id }}">

          </div>
          </div>
          </div>

          <div class="modal-footer">
        <button type="submit" class="btn btn-danger" >Save Changes</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>

        </form>




      </div>

      <!-- Modal footer -->


    </div>
  </div>
</div>
