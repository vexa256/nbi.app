@isset($LeaveApplications)
                @foreach ($LeaveApplications as $data)
<div class="modal"  id="LeaveApplications{{$data->UNI}}">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header ">
        <h5 class="modal-title">View application letter for the selected leave event
        </h5>
        <button type="button" data-toggle="modal" data-target="#MyLeaveApps" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="max-height: 450px ; overflow-y: scroll">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Leave Application Letter</label>
              <textarea name="desc" class="app_letters">
              {{$data->desc}}
              </textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button data-toggle="modal" data-target="#MyLeaveApps" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endforeach
@endisset


 @isset($ApprovedApplications)
                  @foreach ($ApprovedApplications as $data)
<div class="modal"  id="LeaveApplicationsz{{$data->UNI}}">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header ">
        <h5 class="modal-title">View application letter for the selected leave event
        </h5>
        <button type="button" data-toggle="modal" data-target="#MyLeaveApps" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="max-height: 450px ; overflow-y: scroll">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Leave Application Letter</label>
              <textarea name="desc" class="app_letters">
              {{$data->desc}}
              </textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button data-toggle="modal" data-target="#MyLeaveApps" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endforeach
@endisset




   @isset($DeclinedApplications)
                  @foreach ($DeclinedApplications as $data)
<div class="modal"  id="LeaveApplicationszz{{$data->UNI}}">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header ">
        <h5 class="modal-title">View application letter for the selected leave event
        </h5>
        <button type="button" data-toggle="modal" data-target="#MyLeaveApps" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="max-height: 450px ; overflow-y: scroll">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Leave Application Letter</label>
              <textarea name="desc" class="app_letters">
              {{$data->desc}}
              </textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button data-toggle="modal" data-target="#MyLeaveApps" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endforeach
@endisset
