<div class="col-md-12">

	 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Use this interface to view the supervisors assigned to the user <span class="text-danger font-weight-bold">{{$User_Name}}</span> </h3>



      </div>
      <div class="card-body">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <table  class="table filter_a_a table-bordered table-striped " >

            		<thead>
            			<tr >
            				<th class=" bg-primary shadow-lg text-light">User's Name</th>

                     <th class=" bg-dark shadow-lg text-light">Users's Job Title</th>

                    <th class="bg-danger shadow-lg text-light">Supervisor's Name</th>

                    <th class=" bg-primary shadow-lg text-light">Supervisor's Job Title</th>

                    <th class=" bg-primary shadow-lg text-light">Supervisor's Assigned Role</th>


                    <th>Revoke Assignment</th>


            			</tr>
            		</thead>

	<tbody>
            @isset($Supervisors)
               @foreach ($Supervisors as $data)
              <tr >

                  <td>{{$User_Name}}</td>
                  <td>{{$User_Job}}</td>
                  <td>{{$data->Name}}</td>
                  <td>{{$data->JobTitle}}</td>
                  <td>{{$data->role}}</td>




                  <td >
                        <a href="{{ route('RevokeAssignedSupervisor',['id' => $data->UNI]) }}" class=" avhsvshsa btn btn-sm btn-danger text-light shadow-lg">
                              <i class="fas fa-times"></i>
                              Revoke
                        </a>

                  </td>
            </tr>
            @endforeach
            @endisset


	</tbody>

            </table>


</div>
</div>
</div>
