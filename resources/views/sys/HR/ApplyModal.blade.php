<div class="modal"  id="applications_AAAA">
  <div class="modal-dialog modal-dialog modal-xl" >
    <div class="modal-content">
      <div class="modal-header bg-primary text-light">
        <h5 class="modal-title ">Leave Application Interface</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="SSS">
          <form method="POST" action="{{ route('SubmitLeaveApplication') }}">

            @csrf

            <div class="form-group row">
              <div class="col-md-6">
                <label>From Date</label>
                <div class="input-group date" id="from_date" data-target-input="nearest">
                  <input required type="text" name="from_date" class="form-control datetimepicker-input" data-target="#from_date">
                  <div class="input-group-append" data-target="#from_date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
              </div>

              <input type="hidden" name="EmployeeNo" value="{{ Auth::user()->EmployeeNo }}">

              <div class="col-md-6">
                <label>To Date</label>
                <div class="input-group date" id="to_date" data-target-input="nearest">
                  <input required type="text" name="to_date" class="form-control datetimepicker-input" data-target="#to_date">
                  <div class="input-group-append" data-target="#to_date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
              </div>
               <div class="col-md-12 mt-2">
                <label >Please fill in the leave application form  <small class="text-danger">(Please switch the editor to full screen for ease of use)</small></label>
                  <textarea required name="ApplicationLetter" class="app_letters"> <blockquote> <h2><strong>Nile Basin Initiative Secretariat (Nile-SEC)&nbsp;<br /> P.O Box 192 Entebbe, Uganda<br /> Plot 12 Mpigi Road, Entebbe<br /> Tel: +256 (414) 321 424/ +256 (417) 705 000<br /> Email: nbisec@nilebasin.org</strong></h2> </blockquote> <table cellspacing="0" style="border-collapse:collapse; width:617px"> <tbody> <tr> <td style="border-bottom:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000; border-top:1px solid #000000; vertical-align:top; width:617px"> <h1><strong><strong>Leave Application Form</strong></strong></h1> </td> </tr> </tbody> </table> <p>&nbsp;</p> <p>I, _________ , an employee of &nbsp; Nile Basin Initiative<em> </em><em>&nbsp;&nbsp;</em></p> <p>&nbsp;</p> <p>&nbsp;reporting to _______________________, <em><em>&nbsp;(Your </em></em><em><em>supervisor</em></em><em><em>&rsquo;s </em></em><em><em>name)</em></em></p> <p>&nbsp;</p> <p>wish to apply for __________________________&nbsp;days<em><em>&nbsp;</em></em>&nbsp;of leave&nbsp; from <em><em>(start date)</em></em>________________________ to&nbsp;<em><em>(end date)</em></em>____________________________&nbsp; &nbsp;</p> <p>&nbsp;</p> <p>&nbsp;for<em> </em><em> </em>the following reason(s):</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>Applicant&rsquo;s Signature (Type your name)&nbsp; ____________________________________&nbsp;&nbsp;Date_____________________________________</p> <p>&nbsp;</p> <p>&nbsp;</p> <table cellspacing="0" style="border-collapse:collapse; width:434px"> <tbody> <tr> <td colspan="2" style="border-bottom:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000; border-top:1px solid #000000; vertical-align:top; width:433px"> <p><strong><strong>For Official Use</strong></strong></p> </td> </tr> <tr> <td style="border-bottom:1px solid #000000; border-left:1px solid #000000; border-right:none; border-top:none; vertical-align:top; width:209px"> <p>&nbsp;</p> <p>Approved</p> </td> <td style="border-bottom:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000; border-top:1px solid #000000; vertical-align:top; width:224px"> <p>&nbsp;</p> <p>Rejected</p> </td> </tr> </tbody> </table> <p>&nbsp;</p> <p>Signed By:&nbsp;_____________________________________</p> <p>Name:&nbsp;_____________________________________</p> <p>Date:&nbsp;_____________________________________</p> <p>&nbsp;</p> <p>Signed By:&nbsp;_____________________________________</p> <p>Name:&nbsp;_____________________________________</p> <p>Date:&nbsp;_____________________________________</p> <p>&nbsp;</p> <p>Signed By:&nbsp;_____________________________________</p> <p>Name:&nbsp;_____________________________________</p> <p>Date:&nbsp;_____________________________________</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p>

                  </textarea>
              </div>
            </div>




            <div class="modal-footer">



              <button type="submit" class="btn btn-primary">Save changes</button>

              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
