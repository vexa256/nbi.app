@isset($Supervisors)
@foreach ($Supervisors as $data)
<div class="modal"  id="CreateSupervisorWheheh{{$data->id}}">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header ">
        <h5 class="modal-title">Description for the supervisor role in relation to the user
        <span class="text-danger font-weight-bold">
          {{$data->name}}
        </span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="max-height: 450px ; overflow-y: scroll">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Description</label>
              <textarea name="desc" class="app_letters">
              {{$data->desc}}
              </textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
@endforeach
@endisset
