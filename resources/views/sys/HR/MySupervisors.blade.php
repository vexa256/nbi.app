



<div class="modal" tabindex="-1" role="dialog" id="Mysupervisors">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">All your supervisors</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <table  class="table data table-bordered table-striped " >

                <thead>
                  <tr >
                    <th class=" bg-primary shadow-lg text-light">Name</th>
                    <th class=" bg-dark shadow-lg text-light">Job Title</th>


                    <th class="">Role</th>




                  </tr>
                </thead>

  <tbody>
            @isset($ReturnSupervisors)
               @foreach ($ReturnSupervisors as $data)
              <tr >
                  <td class=" bg-primary shadow-lg text-light">{{ $data->Name }}</td>
                  <td class=" bg-dark shadow-lg text-light">{{ $data->JobTitle }}</td>

                  <td >{{ $data->role }}</td>





            </tr>
            @endforeach
            @endisset


  </tbody>

            </table>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
