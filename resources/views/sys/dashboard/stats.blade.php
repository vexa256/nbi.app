<div class="col-md-12">
            <!-- small box -->
            <div class="small-box bg-dark shadow-lg">

              <div class="inner pl-3 pr-3">

                <h3>USD {{ number_format($CountryContributions, 2) }}</h3>

                <p>Total Donor/Country Contributions for the Year <span class="text-danger
                  font-weight-bold">{{ $FilterYear }}</span></p>
              </div>
              <div class="icon">
                <i class="fas fa-calculator fas-2x text-light"></i>
              </div>
              <a href="#" class="small-box-footer">Applicable year is subject to the selected year filter <i class="fas
fa-exclamation-circle"></i></a>
            </div>
          </div>
 @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

<div class="col-md-12">
 <div class="card card-default">
          <div class="card-header">
              <h3 class="card-title">Annual Donor Funding Analysis  for
         the Year <span class="text-danger font-weight-bold">{{ $FilterYear }}</span>
        </h3>
          </div>
          <div class="card-header">
            <form class="row" method="POST" action="{{ route('AnualDonorFundingFilter') }}">
            <div class="col-md-6">

        @csrf  <select required class="form-control " name="FilterYear" >
            <option value="">Filter Years</option>
            @isset($Years)
               @foreach ($Years as $data)

               <option value="{{ $data->Year }}">{{ $data->Year }}</option>

               @endforeach
            @endisset
          </select>
        </div>
   <div class="col-md-2">

          <button type="submit" class="btn btn-sm shadow-lg btn-danger"> <i class="fas fa-search"></i>
          </button>

    </div>
    <div class="col-md-4">

          <a href="#" class=" btn-sm shadow-lg bg-dark">Selected Year ({{ $FilterYear }})</a>

    </div>
</form>
          </div>
          <!-- /.card-header -->
          <div class="card-body" style="max-height: 250px">

                  {!! $chart->container() !!}

          </div>

          <div class="card-footer">

              <a href="#ViewTabular" data-toggle="modal" class="btn btn-primary shadow-lg">

                <i class="fas fa-info-circle "></i>
              Explain</a>

          </div>
      </div>
  </div>


<div class="col-md-12">
 <div class="card card-default">
          <div class="card-header">
              <h3 class="card-title">Monthly Donor Funding Analysis  for
         the Year <span class="text-danger font-weight-bold">{{ $FilterYear }}</span>
        </h3>
          </div>
          <div class="card-header">
      <form class="row" method="POST" action="{{ route('AnualDonorFundingFilter') }}">
            <div class="col-md-6">
        @csrf  <select required class="form-control " name="FilterYear" >
            <option value="">Filter Years </option>
            @isset($Years)
               @foreach ($Years as $data)

               <option value="{{ $data->Year }}">{{ $data->Year }}</option>

               @endforeach
            @endisset
          </select>
        </div>
   <div class="col-md-2">

          <button type="submit" class="btn btn-sm shadow-lg btn-danger">
            <i class="fas fa-search"></i>
          </button>

    </div>
    <div class="col-md-4">

         <a href="#" class="btn-sm shadow-lg btn-dark">Selected Year ({{ $FilterYear }})</a>

    </div>
</form>
          </div>
          <!-- /.card-header -->
          <div class="card-body" style="max-height: 250px">

                  {!! $chart2->container() !!}

          </div>

          <div class="card-footer">

              <a href="#MonthlyAnalysis" data-toggle="modal" class="btn btn-primary shadow-lg">

                <i class="fas fa-info-circle "></i>
              Explain</a>

          </div>
      </div>
  </div>


























<div class="modal" tabindex="-1" role="dialog" id="ViewTabular">
  <div class="modal-dialog modal-lg" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tabular Annual Donor Funding Analysis  for
         the Year <span class="text-danger font-weight-bold">{{ $FilterYear }}</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">

        <div class="card card-default col-md-12">



            <div class="card-body">
                        <table class="filter_a_a table table-dark shadow-lg table-striped table-bordered">
            <thead>
              <tr>
                <th scope="col">Selected Year</th>
                <th scope="col">Donor</th>
                <th scope="col">Amount in USD</th>

              </tr>
            </thead>
            <tbody>

              @isset($dr)
                 @foreach ($dr as $d)

                   <tr>
                    <td>{{ $FilterYear}}</td>
                    <td>{{ $d->Donor }}</td>
                    <td>{{ number_format($d->sum,2) }} USD</td>
                  </tr>


                 @endforeach
              @endisset


            </tbody>
          </table>
            </div>
        </div>


      </div>




      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal" tabindex="-1" role="dialog" id="MonthlyAnalysis">
  <div class="modal-dialog modal-lg" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tabular Monthly Donor Funding Analysis for
         the Year <span class="text-danger font-weight-bold">{{ $FilterYear }}</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">


         <div class="card card-default col-md-12">


            <div class="card-body">
                        <table class="table filter_a_a table-striped table-dark">
            <thead>
              <tr>
                   <th scope="col">Month</th>
                <th scope="col">Amount in USD</th>


              </tr>
            </thead>
            <tbody>

              @isset($dr2)
                 @foreach ($dr2 as $d)

                   <tr>
                       <td>{{ $d->Month }}</td>
                    <td>{{ number_format($d->sum,2) }} USD</td>



                  </tr>


                 @endforeach
              @endisset


            </tbody>
          </table>
            </div>
        </div>
      </div>




      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
