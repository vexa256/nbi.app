 <div class="col-12">
 <div class="card card-default">
          <div class="card-header">
          <h3 class="card-title">
        Select the activity to attach the variance report to and click next

        <span class="text-danger font-weight-bold ml-2">


        </span>

</h3>
            <i class="fas float-right fa-users fa-2x fa-fw"></i>

          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
               <form method="POST" action="{{ route('SelectYearVariance') }}">
                 @csrf
                 <div class="row">
                 <div class="col-md-12">
                 <div class="form-group">

                <input type="hidden" name="Project" value="{{ $GDC1 }}">

                  <label> Select the activity </label>
                  <select required name="Activity" class="getdesc form-control select2bs4" style="width: 100%;">
                     <option selected="selected"></option>
                    @isset($Selections)
                        @foreach ($Selections->unique('Name') as $data)


                    <option value="{{ $data->Code }}">{{ $data->Name}}</option>

                        @endforeach
                    @endisset

                  </select>
                </div>
              </div>

              </div>

                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">

          <button  type="submit" class="btn btn-sm float-right btn-danger shadow-lg">

              <i class="fas fa-check "></i> next step

          </button>

  </form>


          </div>
        </div>
      </div>
