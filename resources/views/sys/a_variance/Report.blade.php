<div class="col-md-12">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title"> Variance Report For The Project
      <span class="text-danger font-weight-bold">{{ $GDC1 }} </span>
      </h3>
      <span class="float-right text-danger font-weight-bold">
       Project/Activity Based Filter tagged to the year {{ $Year }}
      </span>
    </div>




    <div class="card-body">


      <table style="width: 100% !important"  class="table data table-bordered table-primary table-responsive" >
        <thead>
          <tr >
            <th class=" ">Project</th>
            <th class=" ">Activity</th>
            <th class="bg-primary text-light ">Budget</th>
            <th class=" bg-success text-light">Actual Expenditure</th>
            <th class=" bg-danger text-light">Variance</th>
            <th class=" ">Budget Name</th>
            <th class=" ">Tracked Year</th>


          </tr>
        </thead>
        <tbody>
         <tr>
           <td>{{ $GDC1 }}</td>
           <td>{{ $GDC2 }}</td>
           <td class="bg-primary text-light">{{ $Budget }}</td>
           <td class="bg-success text-light">{{ $Actual }}</td>
           <td class=" bg-danger text-light">{{ $Variance }}</td>
           <td>{{ $BudgetName }}</td>
           <td>{{ $Year }}</td>
         </tr>
      </tbody>
    </table>
  </div>
</div>
</div>
<div class="col-md-6">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title"> Selected Variance Report Line Graph

    </div>




    <div class="card-body">

       {!! $chart2->container() !!}

  </div>
</div>
</div>

<div class="col-md-6">
  <div class="card card-default">
    <div class="card-header">
      <h3 class="card-title"> Selected Variance Report Bar Graph

    </div>




    <div class="card-body">

       {!! $chart->container() !!}

  </div>
</div>
</div>
