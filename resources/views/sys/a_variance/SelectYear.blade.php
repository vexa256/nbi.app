 <div class="col-12">
 <div class="card card-dark shadow-lg ">
          <div class="card-header">
            <h3 class="card-title">Select the  Year Context For The Variance Report




            </h3>

          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
               <form method="POST" action="{{ route('GenerateVarianceReport') }}">
                 @csrf
                 <div class="row">

                    <input type="hidden" name="Project" value="{{ $GDC1 }}">
                    <input type="hidden" name="Activity" value="{{ $GDC2 }}">

              <div class="col-md-12">
                 <div class="form-group">

                <label>Select  Applicable Year</label>
                  <select required  name="Year" class="getdesc form-control select2bs4" style="width: 100%;">
                     <option selected="selected"></option>
                    @isset($Selections)
                        @foreach ($Selections as $data)


                    <option>{{ $data['PostingDate']}}</option>

                        @endforeach
                    @endisset

                  </select>



                </div>
              </div>


              </div>

                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">

          <button  type="submit" class="btn btn-sm float-right btn-danger shadow-lg">

              <i class="fas fa-check "></i> Generate Report

          </button>

  </form>


          </div>
        </div>
      </div>
