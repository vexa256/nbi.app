<div class="col-md-12">

	 <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Use this interface to manage generic user roles</h3>

            <a href="#CreateSupervisor" data-toggle="modal" class="btn viewer-only float-right shadow-lg btn-danger">

              <i class="fas fa-plus"></i> New Account

            </a>


      </div>
      <div class="card-body">


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <table  class="table data table-bordered table-striped " >

            		<thead>
            			<tr >
            				<th class=" bg-primary shadow-lg text-light">Name</th>
                    <th class=" bg-primary shadow-lg text-light">Job Title</th>
            				<th>Employee Code</th>

                    <th class="">Role</th>

                    <th>Revoke Previllage</th>


            			</tr>
            		</thead>

	<tbody>
            @isset($Supervisors)
               @foreach ($Supervisors as $data)
              <tr >
                  <td class=" bg-primary shadow-lg text-light">{{ $data->name }}</td>
                  <td class=" bg-primary shadow-lg text-light">{{ $data->JobTitle }}</td>
                  <td >{{ $data->EmployeeNo }}</td>
                  <td >{{ $data->current_team_id }}</td>




                  <td >
                        <a href="{{ route('RevokePrevillage', ['id' => $data->UNI]) }}" class="viewer-only avhsvshsa btn btn-sm btn-danger text-light shadow-lg">
                              <i class="fas fa-trash"></i>
                              Revoke
                        </a>

                  </td>
            </tr>
            @endforeach
            @endisset


	</tbody>

            </table>


</div>
</div>
</div>
@include('sys.users.CreateRole')
