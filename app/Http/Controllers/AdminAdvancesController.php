<?php
namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Illuminate\Http\Request;

class AdminAdvancesController extends Controller
{
  /**
   * @param $PARAMETERS
   */
  public function ReturnYears($PARAMETERS = null)
  {

    $Dates = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1156')

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->select('Entry.Posting Date AS PostingDate')

      ->get();

    foreach ($Dates as $data)
    {
      $s = strtotime($data->PostingDate);

      $Year = date('Y', $s);

      $Years[] = [

        'PostingDate' => $Year

      ];
    }

    $Selections = array_unique($Years, SORT_REGULAR);

    return $Selections;
  }

  /**
   * @param request $request
   */
  public function AdminAdvancesSelectYear(request $request)
  {

    $Cache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1156')

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->select(

        'Entry.G_L Account No_ AS G_L_AccountNo',
        'Entry.Payee AS StaffCode',
        'Entry.Credit Amount AS CreditAmount',
        'Entry.Debit Amount AS DebitAmount',
        'Entry.Description',
        'Entry.Posting Date AS PostingDate',
        'Entry.Entry No_ AS EntryNo',

        'Employee.First Name AS Fname',
        'Employee.Middle Name AS Mname',
        'Employee.Last Name AS Lname',
      )

      ->get()->unique('StaffCode');

    $data = [

      'Page'  => 'sys.AdminAdvances.SelectYear',

      'Years' => $this->ReturnYears(),

      'Users' => $Cache,

      'Title' => 'Administrative Advances Report Generation Interface'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param $StaffCode
   * @param $Year
   */
  public function ReturnDebitAmount($StaffCode, $Year)
  {
    $Sum = 00;

    $Cache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1156')

      ->whereYear('Entry.Posting Date', '=', $Year)

      ->where('Entry.Payee', '=', $StaffCode)

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->sum('Entry.Debit Amount');

    if ($Cache <= 0)
    {
      $Sum = 00;

      return $Sum;
    }
    else
    {

      return $Cache;
    }
  }

  /**
   * @param $StaffCode
   * @param $Year
   */
  public function ReturnCreditAmount($StaffCode, $Year)
  {
    $Sum = 00;

    $Cache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1156')

      ->whereYear('Entry.Posting Date', '=', $Year)

      ->where('Entry.Payee', '=', $StaffCode)

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->sum('Entry.Credit Amount');

    if ($Cache <= 0)
    {
      $Sum = 00;

      return $Sum;
    }
    else
    {

      return $Cache;
    }
  }

  /**
   * @param $StaffCode
   * @param $Year
   */
  public function ReturnLogs($StaffCode, $Year)
  {

    $Cache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1156')

      ->whereYear('Entry.Posting Date', '=', $Year)

      ->where('Entry.Payee', '=', $StaffCode)

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->select(

        'Entry.G_L Account No_ AS G_L_AccountNo',
        'Entry.Payee AS StaffCode',
        'Entry.Credit Amount AS CreditAmount',
        'Entry.Debit Amount AS DebitAmount',
        'Entry.Description',
        'Entry.Posting Date AS PostingDate',
        'Entry.Entry No_ AS EntryNo',

        'Employee.First Name AS Fname',
        'Employee.Middle Name AS Mname',
        'Employee.Last Name AS Lname',
      )

      ->get();

    return $Cache;
  }

  /**
   * @param request $request
   */
  public function RunAdministrativeAdavanceAnalysis(request $request)
  {

    $request->validate([

      'StaffCode' => 'required',
      'Year'      => 'required'

    ]);

    $a = $request->input('StaffCode');
    $b = $request->input('Year');

    $DebitAmount = $this->ReturnDebitAmount($a, $b);
    $CreditAmount = $this->ReturnCreditAmount($a, $b);
    $Logs = $this->ReturnLogs($a, $b);

    $temp = User::where('EmployeeNo', $a)->first();

    $EmployeeName = $temp->name;

    $data = [

      'Page'         => 'sys.AdminAdvances.Report',
      'Title'        => 'Administrative Adavances Report For <span class="btn bg-dark btn-sm ">'.$EmployeeName.'</span>',
      'EmployeeName' => $EmployeeName,
      'CreditAmount' => $CreditAmount,
      'DebitAmount'  => $DebitAmount,
      'Logs'         => $Logs,
      'Year'         => $b

    ];

    return view("sys.view.index", $data);
  }
}
