<?php
namespace App\Http\Controllers;

use App\Models\ProjectData;
use DB;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
  /**
   * @param $value
   */
  public function SetProjectData($value = '')
  {

    $Projects = $Dates = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')
      ->select('Entry.Global Dimension 1 Code AS Project')
      ->where('Entry.Global Dimension 1 Code', '!=', '')
      ->groupBy('Entry.Global Dimension 1 Code')
      ->get();

    $ProjectData = ProjectData::all();

    $data = [

      "Page"        => "sys.projects.SetProjectData",
      "Title"       => "Create and Manage  'About Project' Data",
      "Projects"    => $Projects,
      "ProjectData" => $ProjectData

    ];

    return view('sys.view.index', $data);

  }

  /**
   * @param request $request
   */
  public function CreateProjectData(request $request)
  {

    $request->validate([
      'start_date'            => 'required|date|before:end_date',
      'end_date'              => 'required|date|after:from_date',
      'title'                 => 'required',
      'introduction'          => 'required',
      'hypotheses_objectives' => 'required',
      'Budget_Holder'         => 'required',
      'budget'                => 'required',
      'partners'              => 'required'

    ]);

    $ProjectData = new ProjectData();

    $ProjectData->title = $request->input('title');
    $ProjectData->start_date = $request->input('start_date');
    $ProjectData->end_date = $request->input('end_date');
    $ProjectData->introduction = $request->input('introduction');

    $ProjectData->hypotheses_objectives = $request->input('hypotheses_objectives');
    $ProjectData->Budget_Holder = $request->input('Budget_Holder');
    $ProjectData->partners = $request->input('partners');
    $ProjectData->budget = $request->input('budget');

    $ProjectData->save();

    return redirect()->route('SetProjectData')
      ->with('status', 'New project profile added successfully');
  }

  /**
   * @param $PARAMETERS
   */
  public function SelectProjectToViewSummary($PARAMETERS = null)
  {

    $Projects = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->where('Global Dimension 1 Code', '!=', '')

      ->groupBy('Global Dimension 1 Code')

      ->select('Global Dimension 1 Code AS GD1')

      ->get();

    $data = [

      'Page'       => 'sys.projects.SelectProject',

      'Selections' => $Projects,

      'Title'      => 'Project Summary Generation Interface, Select Project'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function GenerateProjectSummary(request $request)
  {
    $request->validate([

      'Project' => 'required'

    ]);

    $a = $request->input('Project');

    $ProjectData = ProjectData::where('title', $a)->get();

    $data = [

      "Page"        => "sys.projects.ProjectSummary",
      "Title"       => "Project Summary Viewing Interface",
      "ProjectData" => $ProjectData,
      "Project"     => $a

    ];

    return view('sys.view.index', $data);

  }
}
