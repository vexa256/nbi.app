<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class expenditureByMonth extends Controller
{
  /**
   * @param $Category
   */
  public function FilterByMonthCategory($Category = null)
  {
    $Categories = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

      ->join('Nile Basin Initiative$Dimension Value AS L', 'Entry.Category', '=', 'L.Code')

      ->where('L.Dimension Code', '=', 'CATEGORY')

      ->where('Entry.Category', '!=', '')

      ->where('Entry.G_L Account No_', '>=', 5000)

      ->where('Entry.G_L Account No_', '<=', 5999)

      ->select('Entry.Category', 'L.Name', 'Entry.Global Dimension 2 Code AS GD2')

      ->get();

    $data = [

      'Page'       => 'sys.expenditureByMonth.SelectCategory',

      'Selections' => $Categories->unique('Category'),

      'Title'      => 'Expenditure Report Generation Interface (Fliter by Month)'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function FilterByMonthSelectMonth(request $request)
  {

    $request->validate([

      'Category' => 'required'

    ]);

    $Months = [];

    $c = $request->input('Category');

    $Dates = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('Category', $c)

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->select('Posting Date AS PostingDate')

      ->get();

    foreach ($Dates as $data)
    {
      $s = strtotime($data->PostingDate);

      $Month = date('m', $s);

      $Months[] = [

        'PostingDate' => $Month

      ];
    }

    $Selections = array_unique($Months, SORT_REGULAR);

    $data = [

      'Page'       => 'sys.expenditureByMonth.SelectMonth',

      'Selections' => $Selections,

      'Category'   => $c,

      'Title'      => 'Expenditure Report Generation Interface (Fliter by Month)'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function SelectMonth(request $request)
  {

    $request->validate([

      'Category' => 'required',

      'Month'    => 'required'

    ]);

    $c = $request->input('Category');

    $a = $request->input('Month');

    $Projects = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('Category', $c)

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->whereMonth('Posting Date', '=', $a)

      ->groupBy('Global Dimension 1 Code')

      ->select('Global Dimension 1 Code AS GD1')

      ->get();

    $data = [

      'Page'       => 'sys.expenditureByMonth.SelectProject',

      'Selections' => $Projects,

      'Category'   => $c,

      'Month'      => $a,

      'Title'      => 'Expenditure Report Generation Interface (Fliter by Month)'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function SelectProjectFilterByMonth(request $request)
  {

    $request->validate([

      'Category' => 'required',

      'Month'    => 'required',

      'Project'  => 'required'

    ]);

    $a = $request->input('Category');
    $b = $request->input('Month');
    $c = $request->input('Project');

    $Activities = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

      ->where('Entry.Category', $a)

      ->join('Nile Basin Initiative$Dimension Value AS L', 'Entry.Global Dimension 2 Code', '=', 'L.Code')

      ->where('L.Dimension Code', '=', 'INPUT')

      ->where('Entry.Global Dimension 1 Code', '=', $c)

      ->whereMonth('Entry.Posting Date', '=', $b)

      ->where('Entry.G_L Account No_', '>=', 5000)

      ->where('Entry.G_L Account No_', '<=', 5999)

      ->select('Entry.Global Dimension 2 Code AS GD2', 'L.Name')

      ->get();

    $data = [

      'Page'       => 'sys.expenditureByMonth.SelectActivity',

      'Selections' => $Activities->unique('Name'),

      'Category'   => $a,

      'Month'      => $b,

      'Project'    => $c,

      'Title'      => 'Expenditure Report Generation Interface (Fliter by Month)'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param $Cat
   * @param $Yr
   * @param $Proj
   * @param $Act
   */
  public function ReturnTotalExpediture($Cat, $Month, $Proj, $Act)
  {

    $data = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->where('Category', $Cat)

      ->whereMonth('Posting Date', '=', $Month)

      ->where('Global Dimension 1 Code', '=', $Proj)

      ->where('Global Dimension 2 Code', '=', $Act)

      ->select('*')

      ->get();

    return $data;
  }

  /**
   * @param request $request
   */
  public function GenerateReportByMonth(request $request)
  {

    $request->validate([

      'Category' => 'required',

      'Month'    => 'required',

      'Project'  => 'required',

      'Activity' => 'required'

    ]);

    $a = $request->input('Category');
    $b = $request->input('Month');
    $c = $request->input('Project');
    $d = $request->input('Activity');

    $data = [

      'Page'       => 'sys.expenditureByMonth.ReportByMonth',

      'Category'   => $a,

      'Month'      => $b,

      'Project'    => $c,

      'Activity'   => $d,

      'Selections' => $this->ReturnTotalExpediture($a, $b, $c, $d),

      'Title'      => 'Annual Expenditure Dynamic Report Filtered By Month'
    ];

    return view('sys.view.index', $data);
  }
}
