<?php
namespace App\Http\Controllers;

use App\Charts\VarianceChart;
use DB;
use Illuminate\Http\Request;

class VarianceReportController extends Controller
{
  /**
   * @param $PARAMETERS
   */
  public function VarianceReportSelectProject($PARAMETERS = null)
  {
    $Projects = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->where('Global Dimension 1 Code', '!=', '')

      ->groupBy('Global Dimension 1 Code')

      ->select('Global Dimension 1 Code AS GD1')

      ->get();

    $data = [

      'Page'       => 'sys.a_variance.SelectProject',

      'Selections' => $Projects,

      'Title'      => 'Select the project whose variance report is required'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param $PARAMETERS
   */
  public function ReturnActivities($GDC1)
  {

    $data = DB::connection('sqlsrv2')->table('Nile Basin Initiative$Dimension Value AS DV')

      ->join('Nile Basin Initiative$G_L Entry AS GL', 'GL.Global Dimension 2 Code', '=', 'DV.Code')

      ->where('GL.Global Dimension 1 Code', $GDC1)
      ->where('DV.Dimension Code', 'INPUT')
      ->whereYear('GL.Posting Date', '>=', '2017')
      ->where('GL.G_L Account No_', '>=', 5000)

      ->where('GL.G_L Account No_', '<=', 5999)

      ->select('DV.*')->get();

    return $data;
  }

  /**
   * @param $PARAMETERS

  public function ReturnReports($GDC1)
  {

  $data = DB::connection('sqlsrv2')->table('Nile Basin Initiative$Dimension Value AS DV')

  ->join('Nile Basin Initiative$G_L Entry AS GL', 'GL.Global Dimension 1 Code', '=', 'DV.Code')

  ->where('GL.Global Dimension 1 Code', $GDC1)
  ->where('DV.Dimension Code', 'PROJECT')
  ->where('GL.G_L Account No_', '>=', 5000)

  ->where('GL.G_L Account No_', '<=', 5999)

  ->select('DV.*')->get();

  return $data;

  }**/

  public function SelectActivities(request $request)
  {
    $GDC1 = $request->input('Project');
    $Projects = $this->ReturnActivities($GDC1);

    $data = [

      'Page'       => 'sys.a_variance.SelectActivity',

      'Selections' => $Projects,

      'GDC1'       => $GDC1,

      'Title'      => 'Select the activity whose variance report is required'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function SelectYearVariance(request $request)
  {
    $GDC1 = $request->input('Project');
    $GDC2 = $request->input('Activity');

    $Dates = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->where('Global Dimension 1 Code', '=', $GDC1)

      ->where('Global Dimension 2 Code', '=', $GDC2)

    //->whereYear('Posting Date', '=', $a)

      ->select('Posting Date AS PostingDate')

      ->get();

    foreach ($Dates as $data)
    {
      $s = strtotime($data->PostingDate);

      $Month = date('Y', $s);

      $Months[] = [

        'PostingDate' => $Month

      ];
    }

    $Selections = array_unique($Months, SORT_REGULAR);

    $data = [

      'Page'       => 'sys.a_variance.SelectYear',

      'Selections' => $Selections,

      'GDC1'       => $GDC1,

      'GDC2'       => $GDC2,

      'Title'      => 'Select the year to bind the variance report to'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function GenerateVarianceReport(request $request)
  {
    $GDC1 = $request->input('Project');
    $GDC2 = $request->input('Activity');
    $Year = $request->input('Year');

    $aa = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$Dimension Value AS DV')

      ->join('Nile Basin Initiative$G_L Entry AS GL', 'GL.Global Dimension 2 Code', '=', 'DV.Code')

      ->where('GL.Global Dimension 1 Code', $GDC1)
      ->where('GL.Global Dimension 2 Code', $GDC2)
      ->where('DV.Dimension Code', 'INPUT')
      ->whereYear('GL.Posting Date', '=', $Year)
      ->where('GL.G_L Account No_', '>=', 5000)

      ->where('GL.G_L Account No_', '<=', 5999)

      ->select('DV.Name')->first();

    $report = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Budget Entry AS BE')

      ->join('Nile Basin Initiative$G_L Entry AS GL', 'GL.Global Dimension 2 Code', '=', 'BE.Global Dimension 2 Code')

      ->where('GL.Global Dimension 1 Code', $GDC1)

      ->where('GL.Global Dimension 2 Code', $GDC2)

      ->whereYear('GL.Posting Date', '>=', '2017')

      ->whereYear('GL.Posting Date', '=', $Year)

      ->where('GL.G_L Account No_', '>=', 5000)

      ->where('GL.G_L Account No_', '<=', 5999)

      ->select(

        'BE.Budget Name AS BudgetName',
        'BE.Amount AS Budget',
        'GL.Amount AS Actual',

      )->get();

    $Budget = $report->sum('Budget');
    $Actual = $report->sum('Actual');
    $Variance = $Budget - $Actual;
    $BudgetName = $report->pluck('BudgetName')->unique();

    $chart2 = new VarianceChart();
    $chart2->labels(['Budget', 'Actual', 'Variance']);
    $chart2->dataset('Variance Report (USD)', 'line', [$Budget, $Actual, $Variance])
      ->color('green')
      ->backgroundColor('purple');

    $chart = new VarianceChart();
    $chart->labels(['Budget', 'Actual', 'Variance']);
    $chart->dataset('Variance Report (USD)', 'bar', [$Budget, $Actual, $Variance])
      ->color('red')
      ->backgroundColor('blue');

    $data = [

      'Page'       => 'sys.a_variance.Report',

      'report'     => $report,

      'chart2'     => $chart2,

      'chart'      => $chart,

      'GDC1'       => $GDC1,

      'GDC2'       => $aa->Name,

      'Year'       => $Year,

      'BudgetName' => $BudgetName,

      'Budget'     => number_format($Budget, 3),

      'Actual'     => number_format($Actual, 3),

      'Variance'   => number_format($Variance, 3),

      'Title'      => 'Variance Report for the project '.$GDC1.' and activity '.$GDC2.' based on the year '.$Year
    ];

    return view('sys.view.index', $data);
  }
}
