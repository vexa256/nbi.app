<?php
namespace App\Http\Controllers;

use App\Models\AssignSupervisors;
use App\Models\EmployeeLeave;
use App\Models\Supervisors;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class HumanResourceController extends Controller
{
  /**
   * @param $EmployeeNo
   */
  public function GenerateUniqueIds($EmployeeNo = null)
  {

    return uniqid();
  }

  /**
   * @return mixed
   */
  public function ReturnSupervisors()
  {

    $data = DB::connection('sqlite')->table('supervisors AS S')

      ->join('users AS U', 'S.EmployeeNo', '=', 'U.EmployeeNo')

      ->join('employee_leaves AS E', 'U.EmployeeNo', '=', 'E.EmployeeNo')

      ->select('U.*', 'S.*', 'E.*', 'U.id AS UNI', 'S.id AS IDZ')

      ->groupBy('U.EmployeeNo')

      ->get();

    return $data;
  }

  /**
   * @param $d
   */
  public function AssignSupervisor($d = null)
  {

    $Users = User::where('inserted', 'true')->get();

    $Supervisors = DB::connection('sqlite')->table('users AS U')

      ->join('employee_leaves AS E', 'U.EmployeeNo', '=', 'E.EmployeeNo')

      ->select('U.*', 'E.*', 'U.id AS UNI')

      ->get()->unique('name');

    $Eleveted = $this->ReturnSupervisors();

    $data = [

      'Users'       => $Users,

      'Eleveted'    => $Eleveted,

      'Assign'      => 'true',

      'Supervisors' => $Supervisors,

      'Page'        => 'sys.HR.AssignSupervisor',

      'Title'       => 'Assign Human Resource Supervisor'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function AssignSupervisorLogic(request $request)
  {
    $validated = $request->validate([
      'user'       => 'required|integer',
      'supervisor' => 'required|integer'

    ]);

    $GetEmpNo = User::find($request->input('supervisor'));
    $Get_U_EmpNo = User::find($request->input('user'));
    $Supervisor_id = $request->input('supervisor');
    $User_id = $request->input('user');

    $check = AssignSupervisors::where('user', $User_id)
      ->where('supervisor', $Supervisor_id)
      ->count();

    if ($check < 1)
    {
      $AssignSupervisors = new AssignSupervisors();

      $AssignSupervisors->user = $User_id;
      $AssignSupervisors->supervisor = $Supervisor_id;
      $AssignSupervisors->S_EmployeeNo = $GetEmpNo->EmployeeNo;
      $AssignSupervisors->U_EmployeeNo = $Get_U_EmpNo->EmployeeNo;

      $AssignSupervisors->save();

      return redirect()->route('AssignSupervisor')
        ->with('status', 'User assigned supervisor successfully, Click "the View button " to see the supervisors assigned to this particular user');
    }
    else
    {

      return redirect()->route('AssignSupervisor')
        ->with('error_a', 'The selected user and supervisor  already belong to the same assignment group ');
    }
  }

  /**
   * @param request $request
   */
  public function CreateSupervisor(request $request)
  {

    $validated = $request->validate([
      'user' => 'required|unique:supervisors'

    ]);

    $Supervisors = new Supervisors();

    $GetEmpNo = User::find($request->input('user'));

    $Supervisors->role = $request->input('role');
    $Supervisors->desc = $request->input('desc');
    $Supervisors->user = $request->input('user');
    $Supervisors->SupervisorID = $this->GenerateUniqueIds();
    $Supervisors->EmployeeNo = $GetEmpNo->EmployeeNo;
    $Supervisors->status = "active";

    $GetEmpNo->current_team_id = 'supervisor';

    $GetEmpNo->save();

    $Supervisors->save();

    return redirect()->route('ManageSupervisors')
      ->with('status', 'Supervisor role assigned successfully');
  }

  public function ManageSupervisors()
  {
    $Users = User::where('inserted', 'true')->get();

    $Supervisors = $this->ReturnSupervisors();

    $data = [

      'Users'       => $Users,

      'mgthr'       => 'true',

      'Supervisors' => $Supervisors,

      'Page'        => 'sys.HR.ManageSupervisors',

      'Title'       => 'Human Resource Supervisor Managements'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param $id
   */
  public function RevokeSupervisor($id)
  {

    $cc = Supervisors::where('SupervisorID', $id)->first();

    $a = User::where('EmployeeNo', $cc->EmployeeNo)->first();

    $av = User::find($a->id);

    $a->current_team_id = "nbi user";

    $a->save();

    Supervisors::where('SupervisorID', $id)->delete();

    return redirect()->route('ManageSupervisors')
      ->with('status', 'Supervisor role revoked successfully');
  }

  /**
   * @param request $request
   */
  public function ViewAssignedSupervisors(request $request)
  {

    $EmployeeNo = $request->input('UserEmpNo');

    $User_Name = User::where('EmployeeNo', $EmployeeNo)->first();

    $User_Job = EmployeeLeave::where('EmployeeNo', $EmployeeNo)->first();

    $Supervisors = DB::connection('sqlite')->table('assign_supervisors AS A')

      ->join('employee_leaves AS E', 'A.S_EmployeeNo', '=', 'E.EmployeeNo')

      ->join('supervisors AS S', 'A.S_EmployeeNo', '=', 'S.EmployeeNo')

      ->select('A.*', 'E.*', 'S.*', 'A.id AS UNI')

      ->where('A.U_EmployeeNo', $EmployeeNo)

      ->get();

    $data = [

      'User_Name'   => $User_Name->name,

      'User_Job'    => $User_Job->JobTitle,

      'Supervisors' => $Supervisors,

      'Page'        => 'sys.HR.ViewAssigned',

      'Title'       => 'View Human Resource Supervisors assigned to the selected User'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param $id
   */
  public function RevokeAssignedSupervisor($id)
  {

    $AssignSupervisors = AssignSupervisors::find($id);
    $AssignSupervisors->delete();

    return redirect()->route('AssignSupervisor')
      ->with('status', 'Supervisor assignment revoked successfully');
  }
}
