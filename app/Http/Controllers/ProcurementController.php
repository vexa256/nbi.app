<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Mail;
use TIMPDF as PDF;

class ProcurementController extends Controller
{
  /**
   * @param $type
   */
  public function ProcurementSelectProject($Status)
  {
    $desc = null;

    if ($Status == 0)
    {
      $desc = 'Open Contracts';
    }
    elseif ($Status == 1)
    {

      $desc = 'Released Contracts';
    }
    elseif ($Status == 2)
    {

      $desc = 'Pending Approval';
    }

    $Projects = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->where('Global Dimension 1 Code', '!=', '')

      ->groupBy('Global Dimension 1 Code')

      ->select('Global Dimension 1 Code AS GD1')

      ->get();

    $data = [

      'Page'       => 'sys.a_procurement.ProcurementSelectProject',

      'Selections' => $Projects,
      'Status'     => $Status,
      'Desc'       => $desc,

      'Title'      => 'Select project whose contracts are to be tracked'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param $Status
   */
  public function ReturnContracts($Status, $GDC1)
  {
    $data = DB::connection('sqlsrv2')->table('Nile Basin Initiative$Purchase Header AS PH')

      ->join('Nile Basin Initiative$Purchase Line AS PL', 'PH.No_', '=', 'PL.Document No_')

      ->where('PH.Shortcut Dimension 1 Code', $GDC1)
      ->where('PH.Status', $Status)
      ->where('PH.Document Type', 1)
      ->whereYear('PH.Contract Start Date', '>=', '2000')
      ->whereYear('PH.Contract End Date', '>=', '2000')

      ->select(

        'PL.Description',
        'PL.Quantity',
        'PL.Delivery Period',
        'PL.Amount Including VAT AS Amount',
        'PL.Quantity Received',

        'PH.No_',
        'PH.Buy-from Vendor Name',
        'PH.Buy-from Vendor No_',
        'PH.Contract Type',
        'PH.Contract Start Date',
        'PH.Contract End Date',
        'PH.Posting Description AS PD',
        'PH.Currency Code',
        'PL.Category',
        'PH.Donor',
        'PH.Grant No',
        'PH.Donor Output',
        'PH.Status',
        'PH.Status',
        'PH.Shortcut Dimension 1 Code AS GDC1',
        'PH.Shortcut Dimension 2 Code AS GDC2',
        'PH.Id',

      )->get();

    return $data;
  }

  /**
   * @param $Status
   */
  public function ReturnStatus($Status)
  {
    $desc = null;

    if ($Status == 0)
    {
      $desc = 'Open Contracts';
    }
    elseif ($Status == 1)
    {

      $desc = 'Released Contracts';
    }
    elseif ($Status == 2)
    {

      $desc = 'Pending Approval Contracts';
    }

    return $desc;
  }

  /**
   * @param request $request
   */
  public function ProcurementSelectedProject(request $request)
  {

    $Project = $request->Project;

    $Status = $request->Status;

    $Contracts = $this->ReturnContracts($Status, $Project);

    if ($Contracts->count() < 1)
    {
      return redirect()->back()->with('status', 'Filter returned O results , Please try another search/filter query');
    }

    $UI_Status = $this->ReturnStatus($Status);

    $data = [

      'Contracts' => $Contracts,

      'Status'    => $UI_Status,

      'Stat'      => $Status,

      'Assign'    => 'true',

      'Project'   => $Project,

      'Page'      => 'sys.a_procurement.Contract',

      'Title'     => 'Contract Tracking Report for the Project

      '.$Project.' (Only '.$UI_Status.' are shown )'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function GeneratePDF(request $request)
  {

    $Project = $request->Project;

    $Status = $request->Status;

    $Contracts = $this->ReturnContracts($Status, $Project);

    if ($Contracts->count() < 1)
    {
      return redirect()->back()->with('status', 'Filter returned O results , Please try another search/filter query');
    }

    $UI_Status = $this->ReturnStatus($Status);

    $data = [

      'Contracts' => $Contracts,

      'Status'    => $UI_Status,

      'Stat'      => $Status,

      'Assign'    => 'true',

      'Project'   => $Project,

      'Page'      => 'sys.a_procurement.pdf',

      'Title'     => 'Contract Tracking Report for the Project

      '.$Project.' (Only '.$UI_Status.' are shown )'
    ];

    $pdf = PDF::loadView('sys.a_procurement.pdf', $data)->setPaper('a2', 'landscape');

    // download PDF file with download method
    return $pdf->download('pdf_file.pdf');
  }

  /**
   * @param request $request
   * @return mixed
   */
  public function GenerateEmail(request $request)
  {

    $Project = $request->Project;

    $Status = $request->Status;

    $Contracts = $this->ReturnContracts($Status, $Project);

    if ($Contracts->count() < 1)
    {
      return redirect()->back()->with('status', 'Filter returned O results , Please try another search/filter query');
    }

    $UI_Status = $this->ReturnStatus($Status);
    $email = $request->input('email');

    $data = [

      'Contracts' => $Contracts,

      'Status'    => $UI_Status,

      'Stat'      => $Status,

      'Assign'    => 'true',

      'Message_a' => 'Report successfully sent to the email " '.$email.' " as an attachment',

      'Project'   => $Project,

      'Page'      => 'sys.a_procurement.Contract',

      'Title'     => 'Contract Tracking Report for the Project

      '.$Project.' (Only '.$UI_Status.' are shown )'
    ];

    $pdf = PDF::loadView('sys.a_procurement.pdf', $data)->setPaper('a2', 'landscape');

    // download PDF file with download method
    Mail::send('sys.a_procurement.pdf', $data, function ($message) use ($data, $pdf, $email, $Project, $UI_Status)
    {
      $message->to($email)
        ->subject('Contract Tracking Report for the Project '.$Project.' (Only '.$UI_Status.' are shown )')
        ->attachData($pdf->output(), "Report.pdf");
    });

    return view('sys.view.index', $data);
  }
}
