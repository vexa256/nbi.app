<?php
namespace App\Http\Controllers;

use App\Models\HRMail;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserRoles extends Controller
{
  /**
   * @param $PARAMETERS
   */
  public function ManageUsersGeneric($PARAMETERS = null)
  {
    $User = DB::connection('sqlite')->table('users')
      ->where('col', '=', '')
      ->orWhereNull('current_team_id')

      ->get()->unique('name');

    $Supervisors = DB::connection('sqlite')->table('users AS U')

      ->join('employee_leaves AS E', 'E.EmployeeNo', '=', 'U.EmployeeNo')

      ->where('U.current_team_id', '=', 'admin')
      ->orWhere('U.current_team_id', '=', 'viewer')

      ->select('U.*', 'E.*', 'U.id AS UNI')

      ->get();

    $data = [

      'Users'       => $User,

      'Supervisors' => $Supervisors,

      'Page'        => 'sys.users.ManageUsers',

      'Title'       => 'Advanced User Roles Management Console'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function CreateUsersGeneric(request $request)
  {
    $request->validate([
      'role' => 'required',
      'user' => 'required'
    ]);

    $role = $request->input('role');
    $id = $request->input('user');

    $User = User::find($id);

    $User->current_team_id = $role;

    $User->save();

    return redirect()->route('ManageUsersGeneric')
      ->with('status', 'You have successfully created a new user account with the role '.$role);

    return view('sys.view.index', $data);
  }

  /**
   * @param $id
   */
  public function RevokePrevillage($id)
  {
    $User = User::find($id);

    $User->current_team_id = 'nbi user';

    $User->save();

    return redirect()->route('ManageUsersGeneric')
      ->with('status', 'You have successfully revoked the account previllages');
  }

  /**
   * @param request $request
   */
  public function UpdateAccount(request $request)
  {
    $User = User::find(Auth::user()->id);

    $User->password = Hash::make($request->input('password'));

    $User->save();

    return redirect()->route('dashboard')
      ->with('status', 'You have successfully updated your account password');

  }

  /**
   * @param $PARAMETERS
   */
  public function ViewAllUsers($PARAMETERS = null)
  {
    $User = User::where('role', 'nbi user')->get()->unique('name');

    $data = [

      'Supervisors' => $User,

      'Page'        => 'sys.users.ViewAllUsers',

      'Title'       => 'Advanced User  Management Console (User List)'
    ];

    return view('sys.view.index', $data);

  }

  /**
   * @param request $request
   */
  public function CreateHRMail(request $request)
  {

    $HRMail = HRMail::find($request->input('id'));
    $HRMail->mail = $request->input('mail');

    $HRMail->save();

    return redirect()->back()
      ->with('status', 'You have successfully updated the Human Resource Office Email');

  }

  /**
   * @param $PARAMETERS
   */
  public function SetHRmail($PARAMETERS = null)
  {

    $a = HRMail::whereNotNull('mail')->first();

    $data = [

      'data'  => $a,

      'Page'  => 'sys.HR.SetHRmail',

      'Title' => 'Human Resource Office Email'
    ];

    return view('sys.view.index', $data);
  }

  /*****class*******/
}
