<?php
namespace App\Http\Controllers;

use App\Charts\VarianceChart;
use App\Models\CacheSettings;
use App\Models\DonorReports;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;

date_default_timezone_set("Africa/Kampala");

class UsersController extends Controller
{
  public function __construct()
  {
    $RolesStore = [];

    $roles = DB::connection('sqlite')->table('users')
      ->where('current_team_id', 'admin')
      ->orWhere('current_team_id', 'viewer')
      ->orWhere('current_team_id', 'supervisor')
      ->select('current_team_id', 'EmployeeNo')
      ->get();

    $s_a = DB::connection('sqlite')->table('user_roles')->count();

    if ($s_a > 0)
    {
      $backup = DB::connection('sqlite')->table('user_roles')->get();

      foreach ($backup as $data)
      {
        $roles_counter = DB::connection('sqlite')->table('users')

          ->where('EmployeeNo', $data->EmployeeNo)->count();

        if ($roles_counter > 0)
        {
          $UserRoles = User::where('EmployeeNo', $data->EmployeeNo)->first();

          $UserRoles->current_team_id = $data->Role;

          $UserRoles->save();
        }
      }
    }

    foreach ($roles as $data)
    {
      $RolesStore[] = [

        'EmployeeNo' => $data->EmployeeNo,
        'Role'       => $data->current_team_id

      ];
    }

    /***for each****/
    DB::connection('sqlite')->table('user_roles')->truncate();

    DB::connection('sqlite')->table('user_roles')->insert($RolesStore);
  }

/**
 *
 *
 *
 * Cache Timer Trigers
 *
 *
 *
 * ****/
/**
 * @var mixed
 */
  public $TempStore = null;

  public function SetTempstore()
  {

    $CacheSettings = CacheSettings::truncate();
    $UpdateSettings = new CacheSettings();
    $UpdateSettings->Cache = 'true';
    $UpdateSettings->save();

    $search = 'Annual';

    $this->TempStore = DB::connection('sqlsrv2')->table('Nile Basin Initiative$Employee AS Emplo')

      ->join('Nile Basin Initiative$Employee Leave Entries AS Entry', 'Emplo.No_', '=', 'Entry.Employee No_')

      ->where('Entry.Leave Code', 'ANNUAL')
      ->where('Entry.Entry Type', '1')
      ->where('Entry.Description', 'LIKE', '%'.$search.'%')

      ->select(
        'Entry.Employee No_ AS EmployeeNo',
        'Entry.Leave Code AS LeaveCode',
        'Entry.No_ Of Days AS Days_Entitled',
        'Entry.Description AS Days_Entitled_Description',
        'Emplo.First Name AS Fname',
        'Emplo.Middle Name AS Mname',
        'Emplo.Last Name AS Lname',
        'Emplo.Job Title AS JobTitle',
        'Emplo.Mobile Phone No_ AS MobilePhoneNo',
        'Emplo.E-Maild AS EMail_Real',
      )
      ->distinct()
      ->get();
  }

/**
 * @return mixed
 */
  public function RunCacheEmployees()
  {
    DB::connection('sqlite')->table('users')->where('role', 'nbi user')->delete();

    DB::connection('sqlite')->table('employee_leaves')->truncate();

    $this->SetTempstore();

    $Data_Store = [];

    foreach ($this->TempStore as $data)
    {
      $Name = '';

      if ($data->Mname != '')
      {
        $Name = $data->Fname.' '.$data->Mname.' '.$data->Lname;
      }
      else
      {
        $Name = $data->Fname.' '.$data->Lname;
      }

      $Data_Store[] = [

        'EmployeeNo'                => $data->EmployeeNo,
        'LeaveCode'                 => $data->LeaveCode,
        'Days_Entitled'             => $data->Days_Entitled,
        'Days_Entitled_Description' => $data->Days_Entitled_Description,
        'JobTitle'                  => $data->JobTitle,
        'MobilePhoneNo'             => $data->MobilePhoneNo,
        'EMail_Real'                => $data->EMail_Real,
        'Name'                      => $Name

      ];
    }

    /***for each****/

    DB::connection('sqlite')->table('employee_leaves')->insert($Data_Store);

    return $this->RunUserCache();
  }

  public function RunUserCache()
  {
    $Data_Store2 = [];

    foreach ($this->TempStore as $data)
    {
      $Name = '';

      if ($data->Mname != '')
      {
        $Name = $data->Fname.' '.$data->Mname.' '.$data->Lname;
      }
      else
      {
        $Name = $data->Fname.' '.$data->Lname;
      }

      $temp = $result = str_replace('/', '', $data->EmployeeNo);
      $email = $temp.'@nbi.ac';
      $password = Hash::make($temp);

      $user_id = Hash::make($password.'pass'.$email);

      $Data_Store2[] = [

        'name'       => $Name,
        'email'      => $email,
        'password'   => $password,
        'role'       => 'nbi user',
        'real_email' => $data->EMail_Real,
        'user_id'    => $user_id,
        'inserted'   => "true",
        'EmployeeNo' => $data->EmployeeNo,
        'LeaveDays'  => $data->Days_Entitled,
        'role'       => 'nbi user'

      ];
    }

    /***for each****/

    DB::connection('sqlite')->table('users')->insert($Data_Store2);
  }

/**
 *
 *
 *
 * Cache Timer Trigers
 *
 *
 *
 * ****/
  public function Bootstrap()
  {
    $isToday = null;

    $CacheSettings = CacheSettings::count();

    if ($CacheSettings >= 1)
    {
      $Timer = CacheSettings::whereDay('created_at', '!=', '')->first();

      $date = Carbon::parse($Timer->created_at);

      $isToday = $date->isToday();
    }
    else
    {

      $CacheSettings = CacheSettings::truncate();
      $UpdateSettings = new CacheSettings();
      $UpdateSettings->Cache = 'true';
      $UpdateSettings->save();

      return $this->RunCacheEmployees();
    }

    if ($isToday == 0 || $isToday == false)
    {
      $CacheSettings = CacheSettings::truncate();
      $UpdateSettings = new CacheSettings();
      $UpdateSettings->Cache = 'true';
      $UpdateSettings->save();

      return $this->RunCacheEmployees();
    }

    return true;
  }

/**
 * @param $id
 */
  public function DumpCache($id = null)
  {
    $this->RunCacheEmployees();

    return redirect()->back()->with('status', 'Cache disk recreated successfully');
  }

/**
 * @param $PARAMETERS
 */
  public function ConsoleDashboard($PARAMETERS = null)
  {
    $this->Bootstrap();

    if (Auth::user()->current_team_id == 'nbi user'
      || Auth::user()->current_team_id == '')
    {
      return redirect()->route('ApplyForLeave');
    }

    if (Auth::user()->current_team_id == 'supervisor')
    {
      return redirect()->route('ApproveLeave');
    }

    $FilterYear = 2018;

    $G_L_Budget_BudgetName = '2018-19';

    $sf = $G_L_Budget_BudgetName;
    $CalculatedDonorContributions = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Donor')
      ->selectRaw('sum(Amount) as sum, Donor')
      ->pluck('sum');

    $CountryContributions = $CalculatedDonorContributions->sum();

    $dr = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Donor')
      ->selectRaw('sum(Amount) as sum, Donor')

      ->pluck('sum', 'Donor');

    $tb = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Donor')
      ->selectRaw('sum(Amount) as sum, Donor')
      ->get();

    $dr2 = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Month')
      ->selectRaw('sum(Amount) as sum, Month')
      ->pluck('sum', 'Month');

    $tb2 = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Month')
      ->selectRaw('sum(Amount) as sum, Month')
      ->get();

    //  return print_r(json_encode($tb2));

    $chart = new VarianceChart();
    $chart2 = new VarianceChart();
    $chart_a = new VarianceChart();

    $chart->labels($dr->keys());
    $chart->dataset('Annual Donor Funding Analysis ('.$FilterYear.')', 'bar', $dr->values())
      ->color('black')
      ->backgroundColor('#006fe6');

    $chart2->labels($dr2->keys());
    $chart2->dataset('Monthly Funding Analysis ('.$FilterYear.')', 'line', $dr2->values())
      ->color('green')
      ->backgroundColor('purple');

    $Years = DB::connection('sqlite')->table('donor_reports')
      ->select('Year')
      ->distinct()
      ->get();

    $data = [
      'Title'                => 'Donor Funding Graphical/Tabular Analytics Console',
      'CountryContributions' => $CountryContributions,
      'Page'                 => 'sys.dashboard.stats',
      'chart'                => $chart,
      'chart2'               => $chart2,
      'dr'                   => $tb,
      'dr2'                  => $tb2,
      'sf'                   => $sf,
      'BudgetYears'          => $sf,

      'Years'                => $Years,
      'chart_a'              => $chart_a,

      'FilterYear'           => $FilterYear

    ];

    return view("sys.view.index", $data);
  }

/**
 * @param request $request
 */
  public function AnualDonorFundingFilter(request $request)
  {
    $this->Bootstrap();

    $FilterYear = $request->input('FilterYear');

    $G_L_Budget_BudgetName = '2018-19';

    $sf = $G_L_Budget_BudgetName;
    $CalculatedDonorContributions = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Donor')
      ->selectRaw('sum(Amount) as sum, Donor')
      ->pluck('sum');

    $CountryContributions = $CalculatedDonorContributions->sum();

    $dr = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Donor')
      ->selectRaw('sum(Amount) as sum, Donor')
      ->pluck('sum', 'Donor');

    $tb = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Donor')
      ->selectRaw('sum(Amount) as sum, Donor')
      ->get();

    $dr2 = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Month')
      ->selectRaw('sum(Amount) as sum, Month')
      ->pluck('sum', 'Month');

    $tb2 = DonorReports::where("Year", $FilterYear)
      ->where('Donor', '!=', 'C00-ALL')
      ->groupBy('Month')
      ->selectRaw('sum(Amount) as sum, Month')
      ->get();

    //  return print_r(json_encode($tb2));

    $chart = new VarianceChart();
    $chart2 = new VarianceChart();
    $chart_a = new VarianceChart();

    $chart->labels($dr->keys());
    $chart->dataset('Annual Donor Funding Analysis ('.$FilterYear.')', 'bar', $dr->values())
      ->color('black')
      ->backgroundColor('#006fe6');

    $chart2->labels($dr2->keys());
    $chart2->dataset('Monthly Funding Analysis ('.$FilterYear.')', 'line', $dr2->values())
      ->color('green')
      ->backgroundColor('purple');

    $Years = DB::connection('sqlite')->table('donor_reports')
      ->select('Year')
      ->distinct()
      ->get();

    $data = [
      'Title'                => 'Donor Funding Graphical/Tabular Analytics Console',
      'CountryContributions' => $CountryContributions,
      'Page'                 => 'sys.dashboard.stats',
      'chart'                => $chart,
      'chart2'               => $chart2,
      'dr'                   => $tb,
      'dr2'                  => $tb2,
      'sf'                   => $sf,
      'BudgetYears'          => $sf,

      'Years'                => $Years,
      'chart_a'              => $chart_a,

      'FilterYear'           => $FilterYear

    ];

    return view("sys.view.index", $data);
  }
};
{
}
