<?php
namespace App\Http\Controllers;

use App\Models\CacheSettings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

date_default_timezone_set("Africa/Kampala");

class CacheUsersController extends Controller
{
  /**
   * @var mixed
   */
  public $TempStore = null;

  public function SetTempstore()
  {

    $CacheSettings = CacheSettings::truncate();
    $UpdateSettings = new CacheSettings();
    $UpdateSettings->Cache = 'true';
    $UpdateSettings->save();

    $search = 'Annual';

    $this->TempStore = DB::connection('sqlsrv2')->table('Nile Basin Initiative$Employee AS Emplo')

      ->join('Nile Basin Initiative$Employee Leave Entries AS Entry', 'Emplo.No_', '=', 'Entry.Employee No_')

      ->where('Entry.Leave Code', 'ANNUAL')
      ->where('Entry.Entry Type', '1')
      ->where('Entry.Description', 'LIKE', '%'.$search.'%')

      ->select(
        'Entry.Employee No_ AS EmployeeNo',
        'Entry.Leave Code AS LeaveCode',
        'Entry.No_ Of Days AS Days_Entitled',
        'Entry.Description AS Days_Entitled_Description',
        'Emplo.First Name AS Fname',
        'Emplo.Middle Name AS Mname',
        'Emplo.Last Name AS Lname',
        'Emplo.Job Title AS JobTitle',
        'Emplo.Mobile Phone No_ AS MobilePhoneNo',
        'Emplo.E-Maild AS EMail_Real',
      )
      ->distinct()
      ->get();
  }

  /**
   * @return mixed
   */
  public function RunCacheEmployees()
  {
    DB::connection('sqlite')->table('users')->where('role', 'nbi user')->delete();

    DB::connection('sqlite')->table('employee_leaves')->truncate();

    $this->SetTempstore();

    $Data_Store = [];

    foreach ($this->TempStore as $data)
    {
      $Name = '';

      if ($data->Mname != '')
      {
        $Name = $data->Fname.' '.$data->Mname.' '.$data->Lname;
      }
      else
      {
        $Name = $data->Fname.' '.$data->Lname;
      }

      $Data_Store[] = [

        'EmployeeNo'                => $data->EmployeeNo,
        'LeaveCode'                 => $data->LeaveCode,
        'Days_Entitled'             => $data->Days_Entitled,
        'Days_Entitled_Description' => $data->Days_Entitled_Description,
        'JobTitle'                  => $data->JobTitle,
        'MobilePhoneNo'             => $data->MobilePhoneNo,
        'EMail_Real'                => $data->EMail_Real,
        'Name'                      => $Name

      ];
    }

    /***for each****/

    DB::connection('sqlite')->table('employee_leaves')->insert($Data_Store);

    return $this->RunUserCache();
  }

  public function RunUserCache()
  {
    $Data_Store2 = [];

    foreach ($this->TempStore as $data)
    {
      $Name = '';

      if ($data->Mname != '')
      {
        $Name = $data->Fname.' '.$data->Mname.' '.$data->Lname;
      }
      else
      {
        $Name = $data->Fname.' '.$data->Lname;
      }

      $temp = $result = str_replace('/', '', $data->EmployeeNo);
      $email = $temp.'@nbi.ac';
      $password = Hash::make($temp);

      $user_id = Hash::make($password.'pass'.$email);

      $Data_Store2[] = [

        'name'       => $Name,
        'email'      => $email,
        'password'   => $password,
        'role'       => 'nbi user',
        'real_email' => $data->EMail_Real,
        'user_id'    => $user_id,
        'inserted'   => "true",
        'EmployeeNo' => $data->EmployeeNo,
        'LeaveDays'  => $data->Days_Entitled,
        'role'       => 'nbi user'

      ];
    }

    /***for each****/

    DB::connection('sqlite')->table('users')->insert($Data_Store2);

    return redirect()->route('Bootstrap');
  }
}
