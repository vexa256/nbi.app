<?php
namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Illuminate\Http\Request;

class AccountabilityReportByMonthController extends Controller
{
  /**
   * @param $PARAMETERS
   */
  public function ReturnMonths($PARAMETERS = null)
  {
    $Months = [];

    $Dates = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1157')

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->select('Entry.Posting Date AS PostingDate')

      ->get();

    foreach ($Dates as $data)
    {
      $s = strtotime($data->PostingDate);

      $Month = date('m', $s);

      $Months[] = [

        'PostingDate' => $Month

      ];
    }

    $Selections = array_unique($Months, SORT_REGULAR);

    return $Selections;
  }

  /**
   * @param request $request
   */
  public function AccountabilityReportByMonth(request $request)
  {

    $Cache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1157')

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->select(

        'Entry.G_L Account No_ AS G_L_AccountNo',
        'Entry.Payee AS StaffCode',
        'Entry.Credit Amount AS CreditAmount',
        'Entry.Debit Amount AS DebitAmount',
        'Entry.Description',
        'Entry.Posting Date AS PostingDate',
        'Entry.Entry No_ AS EntryNo',

        'Employee.First Name AS Fname',
        'Employee.Middle Name AS Mname',
        'Employee.Last Name AS Lname',
      )

      ->get()->unique('StaffCode');

    $data = [

      'Page'   => 'sys.AccountabilityByMonth.SelectMonth',

      'Months' => $this->ReturnMonths(),

      'Users'  => $Cache,

      'Title'  => 'Monthly Accountability Report  <span class="btn bg-dark btn-sm ">
        Filtered by Month
        </span>'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param $StaffCode
   * @param $Month
   */
  public function ReturnDebitAmount($StaffCode, $Month)
  {
    $Sum = 00;

    $Cache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1157')

      ->whereMonth('Entry.Posting Date', '=', $Month)

      ->where('Entry.Payee', '=', $StaffCode)

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->sum('Entry.Debit Amount');

    if ($Cache <= 0)
    {
      $Sum = 00;

      return $Sum;
    }
    else
    {

      return $Cache;
    }
  }

  /**
   * @param $StaffCode
   * @param $Year
   */
  public function ReturnCreditAmount($StaffCode, $Month)
  {
    $Sum = 00;

    $Cache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1157')

      ->whereMonth('Entry.Posting Date', '=', $Month)

      ->where('Entry.Payee', '=', $StaffCode)

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->sum('Entry.Credit Amount');

    if ($Cache <= 0)
    {
      $Sum = 00;

      return $Sum;
    }
    else
    {

      return $Cache;
    }
  }

  /**
   * @param $StaffCode
   * @param $Month
   */
  public function ReturnLogs($StaffCode, $Month)
  {

    $Cache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

    //->where('Entry.G_L Account No_', '1157')

      ->where('Entry.G_L Account No_', '1157')

      ->whereMonth('Entry.Posting Date', '=', $Month)

      ->where('Entry.Payee', '=', $StaffCode)

      ->join('Nile Basin Initiative$Employee AS Employee', 'Entry.Payee', '=', 'Employee.No_')

      ->select(

        'Entry.G_L Account No_ AS G_L_AccountNo',
        'Entry.Payee AS StaffCode',
        'Entry.Credit Amount AS CreditAmount',
        'Entry.Debit Amount AS DebitAmount',
        'Entry.Description',
        'Entry.Posting Date AS PostingDate',
        'Entry.Entry No_ AS EntryNo',

        'Employee.First Name AS Fname',
        'Employee.Middle Name AS Mname',
        'Employee.Last Name AS Lname',
      )

      ->get();

    return $Cache;
  }

  /**
   * @param request $request
   */
  public function RunAccountabilityReportByMonth(request $request)
  {

    $request->validate([

      'StaffCode' => 'required',
      'Month'     => 'required'

    ]);

    $a = $request->input('StaffCode');
    $b = $request->input('Month');

    $DebitAmount = $this->ReturnDebitAmount($a, $b);
    $CreditAmount = $this->ReturnCreditAmount($a, $b);
    $Logs = $this->ReturnLogs($a, $b);

    $temp = User::where('EmployeeNo', $a)->first();

    $EmployeeName = $temp->name;

    $data = [

      'Page'         => 'sys.AccountabilityByMonth.Report',
      'Title'        => 'Monthly Accountability  Report For <span class="btn bg-dark btn-sm ">'.$EmployeeName.'</span>',
      'EmployeeName' => $EmployeeName,
      'CreditAmount' => $CreditAmount,
      'DebitAmount'  => $DebitAmount,
      'Logs'         => $Logs,
      'Month'        => $b

    ];

    return view("sys.view.index", $data);
  }
}
