<?php
namespace App\Http\Controllers;

use App\Models\ApprovalSettings;
use App\Models\ApproveLeaveApps;
use App\Models\AssignSupervisors;
use App\Models\EmployeeLeave;
use App\Models\LeavDaysBalance;
use App\Models\LeaveApplications;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mail;
use PDF;
use Snowfire\Beautymail\Beautymail;

class LeaveController extends Controller
{
  /**
   * @var mixed
   */
  public $Mailz;
  /**
   * @param $PARAMETERS
   */
  public function __construct($PARAMETERS = null)
  {
    $a = LeaveApplications::all();

    foreach ($a as $b)
    {
      $date = Carbon::parse($b->to_date);

      if ($date->isPast() && $b->status == 'ongoing')
      {
        $cd = LeaveApplications::find($b->id);

        $cd->status = "Leave expired/Utilized";

        $cd->save();

        $Mail_now = User::where('EmployeeNo', $b->U_EmployeeNo)->first();

        $data_user = [

          'Mail_now' => $Mail_now,
          'cd'       => $cd

        ];

        $this->Mailz = $Mail_now;

        if ($Mail_now->real_email != '' || $Mail_now->real_email != null)
        {
          $beautymail = app()->make(Beautymail::class);
          $beautymail->send('sys.mail.LeaveExpire', $data_user, function ($message)
          {
            $message
              ->from('web.nbiportal@gmail.com')
              ->to($this->Mailz->real_email, $this->Mailz->name)
              ->subject('You leave has expired');
          });
        }
      }
    }
  }

/**
 * @return mixed
 */
  public function ReturnSupervisors()
  {

    $id = Auth::user()->EmployeeNo;

    $data = DB::connection('sqlite')->table('assign_supervisors AS S')

      ->join('employee_leaves AS E', 'S.S_EmployeeNo', '=', 'E.EmployeeNo')

      ->join('supervisors AS A', 'S.S_EmployeeNo', '=', 'A.EmployeeNo')

      ->select('S.*', 'E.*', 'A.*')

      ->groupBy('E.EmployeeNo')

      ->get();

    return $data;
  }

/**
 * @param $LeavApps
 */
  public function GetUserApprovedAppslications($LeavApps = null)
  {
    $id = Auth::user()->EmployeeNo;

    $a = LeaveApplications::where('U_EmployeeNo', $id)
      ->where('status', 'ongoing')
      ->orWhere('status', 'approved')
      ->count();

    return $a;
  }

/**
 * @param $LeavApps
 */
  public function GetUserPendingAppslications($LeavApps = null)
  {
    $id = Auth::user()->EmployeeNo;

    $a = LeaveApplications::where('U_EmployeeNo', $id)
      ->where('status', 'pending')->count();

    return $a;
  }

/**
 * @param $LeavApplications
 */
  public function ReturnLeaveApps($LeavApplications = null)
  {

    $a = Auth::user()->EmployeeNo;

    $LeaveApplications = LeaveApplications::where('U_EmployeeNo', $a)->get();

    return $LeaveApplications;
  }

/**
 * @param $LeaveDays
 */
  public function CheckLeaveValidity($LeaveDays = null)
  {
    $EmployeeNo = Auth::user()->EmployeeNo;

    $LeaveApplicationsCounters = LeaveApplications::where('status', 'ongoing')
      ->whereDate('to_date', '<', Carbon::now())->count();

    if ($LeaveApplicationsCounters > 0)
    {
      $a = LeaveApplications::where('status', 'ongoing')
        ->whereDate('to_date', '<', Carbon::now())->first();

      $b = $a->id;

      $c = LeaveApplications::find($b);

      $c->status = 'expired/concluded';

      $c->save();

      return true;
    }
    else
    {
      return true;
    }
  }

/**
 * @param $ApplyForLeave
 */
  public function ApplyForLeave($ApplyForLeave = null)
  {
    $this->CheckLeaveValidity();

    $Apps = $this->ReturnLeaveApps();

    $SupervisorsCount = AssignSupervisors::where('U_EmployeeNo', Auth::user()->EmployeeNo)->count();

    $data = [

      'Page'              => 'sys.HR.ApplyForLeave',

      'ReturnSupervisors' => $this->ReturnSupervisors(),

      'SupervisorsCount'  => $SupervisorsCount,

      'LeaveApplications' => $Apps,

      'Approved'          => $this->GetUserApprovedAppslications(),

      'Pending'           => $this->GetUserPendingAppslications(),

      'Title'             => 'Leave Application Dashboard'
    ];

    return view('sys.view.index', $data);
  }

/**
 * @param request $request
 */
  public function SubmitLeaveApplication(request $request)
  {

    $request->validate([
      'from_date'         => 'required|date|after_or_equal:today',
      'to_date'           => 'required|date|after:from_date',
      'ApplicationLetter' => 'required',
      'EmployeeNo'        => 'required'

    ]);
    $EmployeeNo = $request->input('EmployeeNo');

    $SupervisorsCount = AssignSupervisors::where('U_EmployeeNo', $EmployeeNo)->count();

    $LeaveApplicationsCount = LeaveApplications::where('U_EmployeeNo', $EmployeeNo)
      ->where('status', 'pending')
      ->orWhere('status', 'ongoing')
      ->count();

    $to = Carbon::createFromFormat('m/d/Y', $request->input('to_date'));

    $from = Carbon::createFromFormat('m/d/Y', $request->input('from_date'));

    $LeaveDays = $to->diffInDays($from);

    $Temp = $to.$from.$LeaveDays.$request->input('EmployeeNo');

    $AppID = Hash::make($Temp);

    define("AppIDz", $AppID);

    $DaysBalance = Auth::user()->LeaveDays - $LeaveDays;

    if ($LeaveDays > Auth::user()->LeaveDays)
    {
      return redirect()->route('ApplyForLeave')
        ->with('error_a', 'You have applied for a number of leave days that is greater than your actual available leave days, Please try again  ');
    }
    elseif ($SupervisorsCount < 1)
    {
      return redirect()->route('ApplyForLeave')
        ->with('error_a', 'No supervisors in charge of approving your leave application have been assigned to you. Please contact the system administrator for help ');
    }
    elseif ($LeaveApplicationsCount > 0)
    {
      return redirect()->route('ApplyForLeave')
        ->with('error_a', 'Leave application not submited, You currently have either an application that is pending or on-going leave. You can only submit another leave application after the currently pending application is declined/expired ');
    }
    else
    {
      $LeaveApplications = new LeaveApplications();

      $LeaveApplications->U_EmployeeNo = $EmployeeNo;
      $LeaveApplications->from_date = $from;
      $LeaveApplications->to_date = $to;
      $LeaveApplications->days = $LeaveDays;
      $LeaveApplications->AppID = AppIDz;
      $LeaveApplications->desc = $request->input('ApplicationLetter');

      $LeaveApplications->save();

      $ApproveLeaveApps = new ApproveLeaveApps();

      $ApproveLeaveApps->AppID = AppIDz;
      $ApproveLeaveApps->EmployeeNo = $EmployeeNo;
      $ApproveLeaveApps->SupervisorCount = $SupervisorsCount;

      $ApproveLeaveApps->save();

      $LeavDaysBalance = new LeavDaysBalance();

      $LeavDaysBalance->EmployeeNO = $EmployeeNo;
      $LeavDaysBalance->AppID = AppIDz;
      $LeavDaysBalance->name = Auth::user()->name;
      $LeavDaysBalance->from = $from;
      $LeavDaysBalance->to = $to;
      $LeavDaysBalance->DaysAppliedFor = $LeaveDays;
      $LeavDaysBalance->LeaveDaysBalance = $DaysBalance;

      $LeavDaysBalance->save();

      return redirect()->route('ApplyForLeave')
        ->with('status', 'Your leave application has been successfully sent to your supervisor(s). You will be notified via email once we detect supervisor feedback');
    }

/*****ELSE****/
  }

/**
 * @param $AppID
 */
  public function CleanLeaveDaysCache($AppID)
  {

    $act = LeavDaysBalance::where('AppID', $AppID)->delete();

    return true;
  }

/**
 * @param $AppID
 */
  public function CleanApproveLeaveAppsCache($AppID)
  {
    $act = ApproveLeaveApps::where('AppID', $AppID)->delete();

    return true;
  }

/**
 * @param $id
 */
  public function TerminateLeaveApp($id)
  {

    $TerminateApp = LeaveApplications::find($id);

    if ($TerminateApp->status == 'ongoing')
    {
      return redirect()->route('ApplyForLeave')
        ->with('error_a', 'This leave application was approved and is currently ongoing. It can not be cancelled or terminated. Please contact the HR officer for help');
    }
    else
    {
      $this->CleanLeaveDaysCache($TerminateApp->AppID);

      $this->CleanApproveLeaveAppsCache($TerminateApp->AppID);

      $TerminateApp->delete();

      return redirect()->route('ApplyForLeave')
        ->with('status', 'The selected leave application was terminated successfully');
    }
  }

/**
 * @param $app
 */
  public function ReturnAppsForApproval($app = null)
  {

    $id = Auth::user()->EmployeeNo;

    $data = DB::connection('sqlite')->table('leave_applications AS L')

      ->join('employee_leaves AS E', 'L.U_EmployeeNo', '=', 'E.EmployeeNo')

      ->join('assign_supervisors AS A', 'E.EmployeeNo', '=', 'A.U_EmployeeNo')

      ->where('A.S_EmployeeNo', $id)

      ->where('L.status', 'pending')

      ->select('L.*', 'E.*', 'A.*', 'L.id AS UNI')

      ->groupBy('E.EmployeeNo')

      ->get();

    return $data;
  }

/**
 * @param $app
 */
  public function ReturnApprovedApps($app = null)
  {

    $id = Auth::user()->EmployeeNo;

    $data = DB::connection('sqlite')->table('leave_applications AS L')

      ->join('employee_leaves AS E', 'L.U_EmployeeNo', '=', 'E.EmployeeNo')

      ->join('assign_supervisors AS A', 'E.EmployeeNo', '=', 'A.U_EmployeeNo')

      ->join('approval_settings AS AP', 'AP.EmployeeNO', '=', 'A.S_EmployeeNo')

      ->where('A.S_EmployeeNo', $id)

      ->where('AP.Reaction', 'approved')
      ->orWhere('AP.Reaction', 'Leave expired/Utilized')

      ->select('L.*', 'E.*', 'AP.*', 'A.*', 'L.id AS UNI')

      ->groupBy('E.EmployeeNo')

      ->get();

    return $data;
  }

/**
 * @param $app
 */
  public function ReturnDeclinedApps($app = null)
  {

    $id = Auth::user()->EmployeeNo;

    $data = DB::connection('sqlite')->table('leave_applications AS L')

      ->join('employee_leaves AS E', 'L.U_EmployeeNo', '=', 'E.EmployeeNo')

      ->join('assign_supervisors AS A', 'E.EmployeeNo', '=', 'A.U_EmployeeNo')

      ->join('approval_settings AS AP', 'AP.EmployeeNO', '=', 'A.S_EmployeeNo')

      ->where('A.S_EmployeeNo', $id)

      ->where('AP.Reaction', 'declined')

      ->select('L.*', 'E.*', 'AP.*', 'A.*', 'L.id AS UNI')

      ->groupBy('E.EmployeeNo')

      ->get();

    return $data;
  }

/**
 * @param $LeaveApplications
 */
  public function ApproveLeave($LeaveApplications = null)
  {

    $data = [

      'Page'                 => 'sys.HR.ApproveLeave',

      'LeaveApplications'    => $this->ReturnAppsForApproval(),
      'ApprovedApplications' => $this->ReturnApprovedApps(),
      'DeclinedApplications' => $this->ReturnDeclinedApps(),

      'Title'                => 'Manage Leave Applications'
    ];

    return view('sys.view.index', $data);
  }

/**
 * @param $EmployeeNo
 */
  public function SupervisorsCount($EmployeeNo)
  {

    $a = AssignSupervisors::where('U_EmployeeNo', $EmployeeNo)->count();

    return $a;
  }

/**
 * @param $AppID
 */
  public function ReturnRejctionCount($AppID)
  {

    $aps = ApprovalSettings::where('AppID', $AppID)
      ->where('Reaction', 'declined')
      ->count();

    return $aps;
  }

/**
 * @param $AppID
 */
  public function ReturnApprovalCount($AppID)
  {

    $aps = ApprovalSettings::where('AppID', $AppID)
      ->where('Reaction', 'approved')
      ->count();

    return $aps;
  }

/**
 * @param $AppID
 */
  public function RunApproveOrReject($AppID)
  {
    $la = LeaveApplications::where('AppID', $AppID)->first();

    $EmployeeNo = $la->U_EmployeeNo;

    $a = $this->SupervisorsCount($EmployeeNo);

    $b = $this->ReturnApprovalCount($AppID);

    $c = $this->ReturnRejctionCount($AppID);

    if ($c > 0)
    {
      $la->status = 'declined';

      $la->save();

      $ldb = LeavDaysBalance::where('AppID', $AppID)->first();

      $ldb->delete();

      return 'declined';
    }
    elseif ($a == $b)
    {

      $la->status = 'ongoing';

      $ldb = LeavDaysBalance::where('AppID', $AppID)->first();

      $ldb->status = 'true';

      $ldb->save();

      $la->save();

      return 'ongoing';
    }
    elseif ($a != $b)
    {

      $la->status = 'pending';

      $ldb = LeavDaysBalance::where('AppID', $AppID)->first();

      $ldb->status = 'false';

      $ldb->save();

      $la->save();

      return 'pending';
    }
  }

/**
 * @param $AppID
 */
  public function SendLeaveApprovalMail($AppID)
  {
    $AC = LeaveApplications::where('AppID', $AppID)->first();
    $User = EmployeeLeave::where('EmployeeNo', $AC->U_EmployeeNo)->first();
    $a = $User->EMail_Real;
    $email = $a;

    if ($a == null || $a == null || $a == '')
    {
    }
    else
    {

      $data = [

        'username' => $User->Name,
        'from'     => $AC->from_date,
        'to'       => $AC->to_date,
        'days'     => $AC->days

      ];

      $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
      $beautymail->send('sys.mail.Approval', $data, function ($message)
      {
        $message
          ->from('web.nbiportal@gmail.com')
          ->to($email, $email)
          ->subject('Leave Application Approved !!!');
      });

      return true;
    }
  }

/**
 * @param $AppID
 */
  public function SendLeaveDeclineMail($AppID)
  {
    $AC = LeaveApplications::where('AppID', $AppID)->first();
    $User = EmployeeLeave::where('EmployeeNo', $AC->U_EmployeeNo)->first();
    $a = $User->EMail_Real;
    $email = $a;

    if ($a == null || $a == null || $a == '')
    {
      return false;
    }
    else
    {

      $data = [

        'username' => $User->Name,
        'from'     => $AC->from_date,
        'to'       => $AC->to_date,
        'days'     => $AC->days

      ];

      $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
      $beautymail->send('sys.mail.Decline', $data, function ($message)
      {
        $message
          ->from('web.nbiportal@gmail.com')
          ->to($email, $email)
          ->subject('Leave Application Declined !!!');
      });

      return true;
    }
  }

  /**
   * @param $AppID
   */
  public function SendSeenMail($AppID)
  {
    $AC = LeaveApplications::where('AppID', $AppID)->first();
    $User = EmployeeLeave::where('EmployeeNo', $AC->U_EmployeeNo)->first();
    $a = $User->EMail_Real;
    $email = $a;

    if ($a == null || $a == null || $a == '')
    {
      return false;
    }
    else
    {

      $data = [

        'username' => $User->Name,
        'from'     => $AC->from_date,
        'to'       => $AC->to_date,
        'days'     => $AC->days

      ];

      $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
      $beautymail->send('sys.mail.Seen', $data, function ($message)
      {
        $message
          ->from('web.nbiportal@gmail.com')
          ->to($email, $email)
          ->subject('Leave Application Seen !!!');
      });

      return true;
    }
  }

  /**
   * @param $AppID
   */
  public function SendHRMail($AppID)
  {
    $rty = LeaveApplications::where('AppID', $AppID)->first();
    $dataz = [

      'App' => $rty
    ];

    $HR = HRMail::whereNotNull('mail')->first();

    $pdf = PDF::loadView('sys.mail.AppLetter', $dataz);

    Mail::send('sys.mail.AppLetter_hr', $dataz, function ($message) use ($dataz, $pdf, $HR)
    {
      $message->to($HR->mail)
        ->subject('Leave Application Form|Leave Confirmation Email')
        ->attachData($pdf->output(), "LeaveApplicationForm.pdf");
    });
  }

/**
 * @param $id
 */
  public function ApproveLeaveLogic($id)
  {

    $aps = new ApprovalSettings();

    $param = LeaveApplications::find($id);

    $AppID = $param->AppID;

    $aps->EmployeeNo = Auth::user()->EmployeeNo;

    $aps->AppID = $AppID;

    $aps->Reaction = 'approved';

    $aps->save();

    $fr = $this->RunApproveOrReject($AppID);

    if ($fr == 'ongoing')
    {
      $this->SendHRMail($AppID);
      $this->SendLeaveApprovalMail($AppID);

      return redirect()->route('ApproveLeave')
        ->with('status', 'Leave Applications Approved Successfully');
    }
    elseif ($fr == 'pending')
    {

      $this->SendSeenMail();

      return redirect()->route('ApproveLeave')
        ->with('status', 'You have approved the leave application successfully, This is application is pending approval from other supervisor(s) ');
    }
    elseif ($fr == 'declined')
    {
      $this->SendLeaveDeclineMail($AppID);

      return redirect()->route('ApproveLeave')
        ->with('status', 'You have declined this leave application');
    }

    /** ApproveLeaveApps Ignored **/
  }

/**
 * @param $id
 */
  public function DeclineLeaveLogic($id)
  {

    $aps = new ApprovalSettings();

    $param = LeaveApplications::find($id);

    $AppID = $param->AppID;

    $aps->EmployeeNo = Auth::user()->EmployeeNo;

    $aps->AppID = $AppID;

    $aps->Reaction = 'declined';

    $aps->save();

    $fr = $this->RunApproveOrReject($AppID);

    if ($fr == 'ongoing')
    {
      $this->SendLeaveApprovalMail($AppID);

      return redirect()->route('ApproveLeave')
        ->with('status', 'Leave Applications Approved Successfully');
    }
    elseif ($fr == 'pending')
    {

      return redirect()->route('ApproveLeave')
        ->with('status', 'You have approved the leave application successfully, This is application is pending approval from other supervisor(s) ');
    }
    elseif ($fr == 'declined')
    {
      $this->SendLeaveDeclineMail($AppID);

      return redirect()->route('ApproveLeave')
        ->with('status', 'You have declined this leave application ');
    }

    /** ApproveLeaveApps Ignored **/
  }
};
