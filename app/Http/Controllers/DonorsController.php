<?php
namespace App\Http\Controllers;

use App\Charts\VarianceChart;
use App\Models\CacheSettings;
use App\Models\DonorInvoices;
use App\Models\DonorInvoicesCache;
use App\Models\DonorReports;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

date_default_timezone_set("Africa/Kampala");

class DonorsController extends Controller
{
  /**
   * @return null
   */
  public function RunDonorCache()
  {

    $DataStore = [];

    $TCache = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')
      ->where('G_L Account No_', 4200)
      ->select(

        'Entry No_ AS EntryNo',
        'G_L Account No_ AS G_L_AccountNo',
        'Description',
        'Amount',
        'Posting Date AS PostingDate',
        'Document No_ AS DocumentNo',
        'Donor',

      )->get();

    foreach ($TCache as $d)
    {
      $DataStore[] = [

        'EntryNo'       => $d->EntryNo,
        'G_L_AccountNo' => $d->G_L_AccountNo,
        'Description'   => $d->Description,
        'Amount'        => abs($d->Amount),
        'PostingDate'   => $d->PostingDate,
        'DocumentNo'    => $d->DocumentNo,
        'Donor'         => $d->Donor,
        'Year'          => date('Y', strtotime($d->PostingDate)),
        'Month'         => date('M', strtotime($d->PostingDate))

      ];
    }

    DB::connection('sqlite')->table('donor_reports')->truncate();

    DB::connection('sqlite')->table('donor_reports')->insert($DataStore);

    return true;
  }

  public function Bootstrap()
  {
    $isToday = null;

    $CacheSettings = CacheSettings::count();

    if ($CacheSettings >= 1)
    {
      $Timer = CacheSettings::whereDay('created_at', '!=', '')->first();

      $date = Carbon::parse($Timer->created_at);

      $isToday = $date->isToday();
    }
    else
    {

      $CacheSettings = CacheSettings::truncate();
      $UpdateSettings = new CacheSettings();
      $UpdateSettings->Cache = 'true';
      $UpdateSettings->save();

      $this->RunDonorCache();
    }

    if ($isToday == 0 || $isToday == false)
    {
      $CacheSettings = CacheSettings::truncate();
      $UpdateSettings = new CacheSettings();
      $UpdateSettings->Cache = 'true';
      $UpdateSettings->save();

      $this->RunDonorCache();
    }

    return true;
  }

  /**
   * @param $PARAMETERS
   */
  public function DonorSelectYear($PARAMETERS = null)
  {
    $this->Bootstrap();

    $DonorReports = DonorReports::select('Year')->distinct()->get();

    $data = [

      "Page"  => "sys.donors.SelectYear",
      "Title" => "Donor Report Generation interface. Which year should this report be based on ?",
      "Years" => $DonorReports

    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function GenerateDonorReport(request $request)
  {

    $b = $request->input('DonorYear');

    $a = $request->input('DonorsName');

    $chart = new VarianceChart();

    $dr = DonorReports::where('Donor', '=', $a)
      ->where('Year', '=', $b)
      ->orderBy('created_at', 'asc')
      ->pluck('Amount', 'Month');

    $ac = DonorReports::where('Donor', '=', $a)
      ->where('Year', '=', $b)
      ->sum('Amount');

    $donors = DonorReports::where('Donor', '=', $a)
      ->where('Year', '=', $b)
      ->get();

    $chart->labels($dr->keys());
    $chart->dataset('Monthly Donor Funding', 'bar', $dr->values())
      ->color('red')
      ->backgroundColor('#006fe6');

    $data = [

      "Page"       => "sys.donors.DonorReport",
      "Title"      => 'Advanced donor funding report for the year
			<span class="bg-danger btn shadow-lg text-light">'.$b.'</span>',

      "DonorYear"  => $b,
      "DonorsName" => $a,
      "chart"      => $chart,
      "ac"         => $ac,
      "donors"     => $donors

    ];

    return view("sys.view.index", $data);
  }

  /**
   * @param request $request
   */
  public function DonorSelectName(request $request)
  {

    $request->validate(['UserSelected' => 'DonorYear']);

    $DonorReports = DonorReports::where('Year', '=', $request->input('DonorYear'))
      ->select('Donor')
      ->distinct()
      ->get();

    $data = [

      "Page"      => "sys.donors.SelectDonor",
      "Title"     => "Donor Report Generation interface. Which donor should this report be based on ?",
      "Donors"    => $DonorReports,
      "DonorYear" => $request->input('DonorYear')

    ];

    return view("sys.view.index", $data);
  }

  /**
   * @param $Donor
   * @param $Year
   */
  public function RunCache()
  {

    $DonorInvoices = DonorInvoices::get();

    DB::connection('sqlite')->table('donor_invoices_caches')->truncate();

    foreach ($DonorInvoices as $d)
    {
      $DonorSum = DB::connection('sqlite')->table('donor_reports AS DR')

        ->join('donor_invoices AS DI', 'DI.Donor', '=', 'DR.Donor')
        ->where('DI.Donor', '=', $d->Donor)
        ->where('DI.Year', '=', $d->Year)
        ->sum('DR.Amount');

      $Outstanding = $d->InvoicedAmount - $DonorSum;

      $DonorInvoicesCache = new DonorInvoicesCache();

      $DonorInvoicesCache->Donor = $d->Donor;
      $DonorInvoicesCache->Year = $d->Year;
      $DonorInvoicesCache->InvoicedAmount = $d->InvoicedAmount;
      $DonorInvoicesCache->Outstanding = $Outstanding;
      $DonorInvoicesCache->CurrentAmount = $DonorSum;

      $DonorInvoicesCache->save();
    }
  }

  /**
   * @param $PARAMETERS
   */
  public function SetBenchMark($PARAMETERS = null)
  {

    $count = DonorInvoices::count();

    if ($count > 0)
    {
      $this->RunCache();
    }

    $DonorInvoices = DonorInvoicesCache::all();

    $Years = DonorReports::select('Year')->distinct()->get();

    $DonorReports = DonorReports::select('Donor')->get()->unique('Donor');

    $data = [

      "Page"         => "sys.donors.InvoiceDonor",
      "Title"        => "Set donor contribution benchmarks for calculating outstanding balances",
      "Donors"       => $DonorInvoices,
      "DonorReports" => $DonorReports,
      "Years"        => $Years

    ];

    return view("sys.view.index", $data);
  }

  /**
   * @param request $request
   */
  public function SetNewBenchMark(request $request)
  {

    $request->validate([

      'Donor'          => 'required',
      'InvoicedAmount' => 'required',
      'Year'           => 'required'

    ]);

    $Donor = $request->input('Donor');
    $InvoicedAmount = $request->input('InvoicedAmount');
    $Year = $request->input('Year');

    $DonorInvoices = new DonorInvoices();

    $DonorInvoices->Donor = $Donor;
    $DonorInvoices->InvoicedAmount = $InvoicedAmount;
    $DonorInvoices->Year = $Year;

    $DonorInvoices->save();

    return redirect()->route('SetBenchMark')
      ->with('status', 'New donor contribution benchmark set successfully ');
  }

  /**
   * @param $id
   */
  public function DeleteBenchMark($id)
  {

    $d = DonorInvoicesCache::find($id);

    DonorInvoicesCache::where('Donor', $d->Donor)
      ->where('Year', $d->Year)
      ->where('InvoicedAmount', $d->InvoicedAmount)
      ->delete();

    DonorInvoices::where('Donor', $d->Donor)
      ->where('Year', $d->Year)
      ->where('InvoicedAmount', $d->InvoicedAmount)
      ->delete();

    $d->delete();

    return redirect()->route('SetBenchMark')
      ->with('status', 'Donor contribution benchmark deleted successfully ');
  }

  /**
   * @param $PARAMETERS
   */
  public function ViewOutstandingDonor($PARAMETERS = null)
  {

    $count = DonorInvoices::count();

    if ($count > 0)
    {
      $this->RunCache();
    }

    $DonorInvoices = DonorInvoicesCache::all();

    $Years = DonorReports::select('Year')->distinct()->get();

    $DonorReports = DonorReports::select('Donor')->get()->unique('Donor');

    $data = [

      "Page"         => "sys.donors.OutStanding",
      "Title"        => "Donor contribution benchmarks for calculating outstanding balances",
      "Donors"       => $DonorInvoices,
      "DonorReports" => $DonorReports,
      "Years"        => $Years

    ];

    return view("sys.view.index", $data);
  }

  /**
   * @param $PARAMETERS
   */
  public function DonoroAnalyticsSelectYear($PARAMETERS = null)
  {

    $a = DonorInvoicesCache::select('Year')->distinct()->get();

    $count = DonorInvoices::count();

    if ($count > 0)
    {
      $this->RunCache();
    }

    $data = [

      "Page"  => "sys.donors._SelectYear",
      "Title" => "Select the year in relation to the donor contribution analysis report
      (Invoiced VS Paid)",

      "Years" => $a

    ];

    return view("sys.view.index", $data);
  }

  /**
   * @param request $requests
   */
  public function SelectDonorAnalysisYear(request $request)
  {
    $request->validate([

      'Year' => 'required'

    ]);

    $a = $request->input('Year');

    $count = DonorInvoices::count();

    if ($count > 0)
    {
      $this->RunCache();
    }

    $Donors = DonorInvoicesCache::where('Year', $a)->select('Donor')->distinct()->get();

    // return dd($Donors);

    $data = [

      "Page"      => "sys.donors._SelectDonor",
      "Title"     => "Select the donor in relation to the donor contribution analysis report
      (Invoiced VS Paid)",

      "Donors"    => $Donors,
      "DonorYear" => $a

    ];

    return view("sys.view.index", $data);
  }

  /**
   * @param request $request
   */
  public function GenDonorReport(request $request)
  {

    $request->validate([

      'DonorYear'  => 'required',

      'DonorsName' => 'required'

    ]);

    $a = $request->input('DonorYear');
    $b = $request->input('DonorsName');

    $Donors = DonorInvoicesCache::where('Year', $a)->where('Donor', $b)
      ->get();

    $data = [

      "Page"      => "sys.donors._Report",

      "Title"     => "Donor report -- Invoiced VS Paid Amount (Annually) - ".$a,

      "Donors"    => $Donors,

      "DonorYear" => $a

    ];

    return view("sys.view.index", $data);
  }
}
