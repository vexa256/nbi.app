<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class ExpenditureReportByYear extends Controller
{
  /**
   * @param $data
   */
  public function SelectExpenditureCategory($data = null)
  {
    $Categories = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

      ->join('Nile Basin Initiative$Dimension Value AS L', 'Entry.Category', '=', 'L.Code')

      ->where('L.Dimension Code', '=', 'CATEGORY')

      ->where('Entry.Category', '!=', '')

      ->where('Entry.G_L Account No_', '>=', 5000)

      ->where('Entry.G_L Account No_', '<=', 5999)

      ->select('Entry.Category', 'L.Name', 'Entry.Global Dimension 2 Code AS GD2')

      ->get();

    //return dd($Categories->unique('Category'));

    $data = [

      'Page'       => 'sys.expenditureByYear.SelectCategory',

      'Selections' => $Categories->unique('Category'),

      'Title'      => 'Expenditure Report Generation Interface'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function CategorySelected(request $request)
  {

    $request->validate([

      'Category' => 'required'

    ]);

    $Years = [];

    $c = $request->input('Category');

    $Dates = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('Category', $c)

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->select('Posting Date AS PostingDate')

      ->get();

    foreach ($Dates as $data)
    {
      $s = strtotime($data->PostingDate);

      $Year = date('Y', $s);

      $Years[] = [

        'PostingDate' => $Year

      ];
    }

    $Selections = array_unique($Years, SORT_REGULAR);

    $data = [

      'Page'       => 'sys.expenditureByYear.SelectYear',

      'Selections' => $Selections,

      'Category'   => $c,

      'Title'      => 'Expenditure Report Generation Interface'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function SelectYear(request $request)
  {

    $request->validate([

      'Category' => 'required',

      'Year'     => 'required'

    ]);

    $c = $request->input('Category');

    $a = $request->input('Year');

    $Projects = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('Category', $c)

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->whereYear('Posting Date', '=', $a)

      ->groupBy('Global Dimension 1 Code')

      ->select('Global Dimension 1 Code AS GD1')

      ->get();

    $data = [

      'Page'       => 'sys.expenditureByYear.SelectProject',

      'Selections' => $Projects,

      'Category'   => $c,

      'Year'       => $a,

      'Title'      => 'Expenditure Report Generation Interface'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param request $request
   */
  public function SelectProject(request $request)
  {

    $request->validate([

      'Category' => 'required',

      'Year'     => 'required',

      'Project'  => 'required'

    ]);

    $a = $request->input('Category');
    $b = $request->input('Year');
    $c = $request->input('Project');

    $Activities = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry AS Entry')

      ->where('Entry.Category', $a)

      ->join('Nile Basin Initiative$Dimension Value AS L', 'Entry.Global Dimension 2 Code', '=', 'L.Code')

      ->where('L.Dimension Code', '=', 'INPUT')

      ->where('Entry.Global Dimension 1 Code', '=', $c)

      ->whereYear('Entry.Posting Date', '=', $b)

      ->where('Entry.G_L Account No_', '>=', 5000)

      ->where('Entry.G_L Account No_', '<=', 5999)

    //->groupBy('Entry.Global Dimension 2 Code')

      ->select('Entry.Global Dimension 2 Code AS GD2',

        'L.Name'

      )

      ->get();

    $data = [

      'Page'       => 'sys.expenditureByYear.SelectActivity',

      'Selections' => $Activities,

      'Category'   => $a,

      'Year'       => $b,

      'Project'    => $c,

      'Title'      => 'Expenditure Report Generation Interface'
    ];

    return view('sys.view.index', $data);
  }

  /**
   * @param $Cat
   * @param $Yr
   * @param $Proj
   * @param $Act
   */
  public function ReturnTotalExpediture($Cat, $Year, $Proj, $Act)
  {

    $data = DB::connection('sqlsrv2')

      ->table('Nile Basin Initiative$G_L Entry')

      ->where('G_L Account No_', '>=', 5000)

      ->where('G_L Account No_', '<=', 5999)

      ->where('Category', $Cat)

      ->whereYear('Posting Date', '=', $Year)

      ->where('Global Dimension 1 Code', '=', $Proj)

      ->where('Global Dimension 2 Code', '=', $Act)

      ->select('*')

      ->get();

    return $data;
  }

  /**
   * @param request $request
   */
  public function GenerateReportByYear(request $request)
  {

    $request->validate([

      'Category' => 'required',

      'Year'     => 'required',

      'Project'  => 'required',

      'Activity' => 'required'

    ]);

    $a = $request->input('Category');
    $b = $request->input('Year');
    $c = $request->input('Project');
    $d = $request->input('Activity');

    $data = [

      'Page'       => 'sys.expenditureByYear.ReportByYear',

      'Category'   => $a,

      'Year'       => $b,

      'Project'    => $c,

      'Activity'   => $d,

      'Selections' => $this->ReturnTotalExpediture($a, $b, $c, $d),

      'Title'      => 'Annual Expenditure Dynamic Report'
    ];

    return view('sys.view.index', $data);
  }
}
