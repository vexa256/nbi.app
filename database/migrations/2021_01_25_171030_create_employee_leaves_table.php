<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeLeavesTable extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_leaves');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_leaves', function (Blueprint $table) {
            $table->id();
            $table->string('Name')->nullable();
            $table->string('EmployeeNo')->nullable();
            $table->string('LeaveCode')->nullable();
            $table->string('Days_Entitled_Description')->nullable();
            $table->bigInteger('Days_Entitled')->nullable();
            $table->string('MobilePhoneNo')->nullable();
            $table->string('JobTitle')->nullable();
            $table->string('EMail_Real')->nullable();
            $table->date('StartDate')->nullable();
            $table->date('EndDate')->nullable();
            $table->bigInteger('Days_Consumed')->nullable();
            $table->bigInteger('Days_Balance ')->nullable();

            $table->string('Validity')->nullable();

            $table->timestamps();
        });
    }
}
