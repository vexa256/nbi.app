<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorReportsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('donor_reports', function (Blueprint $table)
    {
      $table->id();
      $table->bigInteger('EntryNo')->nullable();
      $table->string('G_L_AccountNo')->nullable();
      $table->string('Description')->nullable();
      $table->bigInteger('Amount')->nullable();
      $table->date('PostingDate')->nullable();
      $table->string('DocumentNo')->nullable();
      $table->string('Donor')->nullable();
      $table->date('Year')->nullable();
      $table->date('Month')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('donor_reports');
  }
}
