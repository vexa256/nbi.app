<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApproveLeaveAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approve_leave_apps', function (Blueprint $table)
        {
            $table->id();
            $table->string('AppID');
            $table->string('EmployeeNo');
            $table->string('status')->default('true');
            $table->bigInteger('SupervisorCount')->default(0);
            $table->bigInteger('ApprovalCount')->default(0);
            $table->bigInteger('RejcectionCount')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approve_leave_apps');
    }
}
