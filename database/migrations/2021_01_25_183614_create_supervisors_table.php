<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupervisorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supervisors', function (Blueprint $table)
        {
            $table->id();
            $table->bigInteger('user');
            $table->string('role')->default('supervisor');
            $table->text('desc')->nullable();
            $table->string('status')->nullable();
            $table->string('SupervisorID')->unique();
            $table->string('EmployeeNo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supervisors');
    }
}
