<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavDaysBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leav_days_balances', function (Blueprint $table)
        {
            $table->id();
            $table->string('EmployeeNO');
            $table->string('AppID');
            $table->string('name');
            $table->date('from');
            $table->date('to');
            $table->bigInteger('DaysAppliedFor');
            $table->string('LeaveCode')->default('ANNUAL');
            $table->string('status')->default('false');
            $table->bigInteger('LeaveDaysBalance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leav_days_balances');
    }
}
