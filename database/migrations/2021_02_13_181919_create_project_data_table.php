<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectDataTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('project_data', function (Blueprint $table)
    {
      $table->id();
      $table->string('title');
      $table->text('introduction');
      $table->text('hypotheses_objectives');
      $table->text('partners');
      $table->date('start_date');
      $table->date('end_date');
      $table->text('Budget_Holder');
      $table->bigInteger('budget');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('project_data');
  }
}
