<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table)
    {
      $table->id();
      $table->string('name')->nullable();
      $table->string('email')->nullable();
      $table->timestamp('email_verified_at')->nullable();
      $table->string('password')->nullable();
      $table->bigInteger('LeaveDays')->nullable();
      $table->rememberToken();
      $table->string('current_team_id')->default('nbi user');
      $table->text('profile_photo_path')->nullable();
      $table->string('role')->nullable();
      $table->string('EmployeeNo')->nullable();
      $table->string('user_id')->nullable();
      $table->string('real_email')->nullable();
      $table->string('inserted')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('users');
  }
}
