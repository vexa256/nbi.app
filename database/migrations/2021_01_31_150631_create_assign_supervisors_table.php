<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignSupervisorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_supervisors', function (Blueprint $table) {

            $table->id();
            $table->bigInteger('user');
            $table->bigInteger('supervisor');
            $table->string('S_EmployeeNo');
            $table->string('U_EmployeeNo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_supervisors');
    }
}
