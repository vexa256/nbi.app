<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorInvoicesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('donor_invoices', function (Blueprint $table)
    {
      $table->id();
      $table->string('Donor');
      $table->string('Year')->nullable();
      $table->bigInteger('InvoicedAmount')->nullable();
      $table->bigInteger('Outstanding')->nullable();
      $table->string('status')->default('true');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('donor_invoices');
  }
}
