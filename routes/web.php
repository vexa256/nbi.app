<?php

use App\Http\Controllers\AccountabilityReportByMonthController;
use App\Http\Controllers\AccountabilityReportByYearController;
use App\Http\Controllers\AdminAdvancesbyMonthController;
use App\Http\Controllers\AdminAdvancesController;
use App\Http\Controllers\CacheUsersController;
use App\Http\Controllers\DonorsController;
use App\Http\Controllers\expenditureByMonth;
use App\Http\Controllers\ExpenditureReportByYear;
use App\Http\Controllers\HumanResourceController;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\ProcurementController;
use App\Http\Controllers\ProjectsController;
use App\Http\Controllers\UserRoles;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\VarianceReportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
require __DIR__.'/auth.php';

Route::post('/GenerateVarianceReport', [VarianceReportController::class, 'GenerateVarianceReport'])->name('GenerateVarianceReport')->middleware("auth");

Route::post('/SelectYearVariance', [VarianceReportController::class, 'SelectYearVariance'])->name('SelectYearVariance')->middleware("auth");

Route::post('/SelectActivities', [VarianceReportController::class, 'SelectActivities'])->name('SelectActivities')->middleware("auth");

Route::get('/VarianceReportSelectProject', [VarianceReportController::class, 'VarianceReportSelectProject'])->name('VarianceReportSelectProject')->middleware("auth");

Route::post('/GenerateEmail', [ProcurementController::class, 'GenerateEmail'])->name('GenerateEmail')->middleware("auth");

Route::post('/GeneratePDF', [ProcurementController::class, 'GeneratePDF'])->name('GeneratePDF')->middleware("auth");

Route::post('/ProcurementSelectedProject', [ProcurementController::class, 'ProcurementSelectedProject'])->name('ProcurementSelectedProject')->middleware("auth");

Route::get('/ProcurementSelectProject/{Status}', [ProcurementController::class, 'ProcurementSelectProject'])->name('ProcurementSelectProject')->middleware("auth");

Route::get('/SetHRmail', [UserRoles::class, 'SetHRmail'])->name('SetHRmail')->middleware("auth");

Route::post('/CreateHRMail', [UserRoles::class, 'CreateHRMail'])->name('CreateHRMail')->middleware("auth");

Route::get('/ViewAllUsers', [UserRoles::class, 'ViewAllUsers'])->name('ViewAllUsers')->middleware("auth");

Route::post('/UpdateAccount', [UserRoles::class, 'UpdateAccount'])->name('UpdateAccount')->middleware("auth");

Route::get('/RevokePrevillage/{id}', [UserRoles::class, 'RevokePrevillage'])->name('RevokePrevillage')->middleware("auth");

Route::post('/CreateUsersGeneric', [UserRoles::class, 'CreateUsersGeneric'])->name('CreateUsersGeneric')->middleware("auth");

Route::get('/ManageUsersGeneric', [UserRoles::class, 'ManageUsersGeneric'])->name('ManageUsersGeneric')->middleware("auth");

Route::get('/SelectProjectToViewSummary', [ProjectsController::class, 'SelectProjectToViewSummary'])->name('SelectProjectToViewSummary')->middleware("auth");
Route::post('/GenerateProjectSummary', [ProjectsController::class, 'GenerateProjectSummary'])->name('GenerateProjectSummary')->middleware("auth");

Route::post('/CreateProjectData', [ProjectsController::class, 'CreateProjectData'])->name('CreateProjectData')->middleware("auth");

Route::get('/SetProjectData', [ProjectsController::class, 'SetProjectData'])->name('SetProjectData')->middleware("auth");

Route::post('/AnualDonorFundingFilter', [UsersController::class, 'AnualDonorFundingFilter'])->name('AnualDonorFundingFilter')->middleware("auth");

Route::get('/', [UsersController::class, 'ConsoleDashboard'])->name('ConsoleDashboard')->middleware("auth");

Route::get('/DumpCache', [UsersController::class, 'DumpCache'])->name('DumpCache')->middleware("auth");

Route::get('/ConsoleDashboard', [UsersController::class, 'ConsoleDashboard'])->name('home')->middleware("auth");

Route::get('/dashboard', [UsersController::class, 'ConsoleDashboard'])->name('dashboard')->middleware("auth");

/**Donors Logic ***/

Route::post('GenDonorReport', [DonorsController::class, 'GenDonorReport'])->name('GenDonorReport')->middleware("auth");

Route::post('SelectDonorAnalysisYear', [DonorsController::class, 'SelectDonorAnalysisYear'])->name('SelectDonorAnalysisYear')->middleware("auth");

Route::get('DonoroAnalyticsSelectYear', [DonorsController::class, 'DonoroAnalyticsSelectYear'])->name('DonoroAnalyticsSelectYear')->middleware("auth");

Route::get('ViewOutstandingDonor', [DonorsController::class, 'ViewOutstandingDonor'])->name('ViewOutstandingDonor')->middleware("auth");

Route::get('/DeleteBenchMark/{id}', [DonorsController::class, 'DeleteBenchMark'])->name('DeleteBenchMark')->middleware("auth");

Route::post('/SetNewBenchMark', [DonorsController::class, 'SetNewBenchMark'])->name('SetNewBenchMark')->middleware("auth");

Route::get('/SetBenchMark', [DonorsController::class, 'SetBenchMark'])->name('SetBenchMark')->middleware("auth");

Route::post('/GenerateDonorReport', [DonorsController::class, 'GenerateDonorReport'])->name('GenerateDonorReport')->middleware("auth");

Route::post('/DonorSelectName', [DonorsController::class, 'DonorSelectName'])->name('DonorSelectName')->middleware("auth");

Route::get('/DonorSelectYear', [DonorsController::class, 'DonorSelectYear'])->name('DonorSelectYear')->middleware("auth");

/**Donors Logic ***/

Route::post('/RunAccountabilityByYearAnalysis', [AccountabilityReportByYearController::class, 'RunAccountabilityByYearAnalysis'])->name('RunAccountabilityByYearAnalysis')->middleware("auth");

Route::get('/AccountabilityByYear', [AccountabilityReportByYearController::class, 'AccountabilityByYear'])->name('AccountabilityByYear')->middleware("auth");

Route::get('/AccountabilityReportByMonth', [AccountabilityReportByMonthController::class, 'AccountabilityReportByMonth'])->name('AccountabilityReportByMonth')->middleware("auth");
Route::post('/RunAccountabilityReportByMonth', [AccountabilityReportByMonthController::class, 'RunAccountabilityReportByMonth'])->name('RunAccountabilityReportByMonth')->middleware("auth");

Route::post('RunAdministrativeAdavanceAnalysisByMonth', [AdminAdvancesbyMonthController::class, 'RunAdministrativeAdavanceAnalysisByMonth'])->name('RunAdministrativeAdavanceAnalysisByMonth')->middleware("auth");

Route::get('SelectAdminAdvancesMonth', [AdminAdvancesbyMonthController::class, 'SelectAdminAdvancesMonth'])->name('SelectAdminAdvancesMonth')->middleware("auth");

Route::post('RunAdministrativeAdavanceAnalysis', [AdminAdvancesController::class, 'RunAdministrativeAdavanceAnalysis'])->name('RunAdministrativeAdavanceAnalysis')->middleware("auth");

Route::get('AdminAdvancesSelectYear', [AdminAdvancesController::class, 'AdminAdvancesSelectYear'])->name('AdminAdvancesSelectYear')->middleware("auth");

Route::post('GenerateReportByMonth', [expenditureByMonth::class, 'GenerateReportByMonth'])->name('GenerateReportByMonth')->middleware("auth");

Route::post('SelectProjectFilterByMonth', [expenditureByMonth::class, 'SelectProjectFilterByMonth'])->name('SelectProjectFilterByMonth')->middleware("auth");

Route::post('FilterByMonthSelectMonth', [expenditureByMonth::class, 'FilterByMonthSelectMonth'])->name('FilterByMonthSelectMonth')->middleware("auth");

Route::post('SelectMonth', [expenditureByMonth::class, 'SelectMonth'])->name('SelectMonth')->middleware("auth");

Route::get('FilterByMonthCategory', [expenditureByMonth::class, 'FilterByMonthCategory'])->name('FilterByMonthCategory')->middleware("auth");

Route::post('GenerateReportByYear', [ExpenditureReportByYear::class, 'GenerateReportByYear'])->name('GenerateReportByYear')->middleware("auth");

Route::post('SelectProject', [ExpenditureReportByYear::class, 'SelectProject'])->name('SelectProject')->middleware("auth");

Route::get('SelectExpenditureCategory', [ExpenditureReportByYear::class, 'SelectExpenditureCategory'])->name('SelectExpenditureCategory')->middleware("auth");

Route::post('CategorySelected', [ExpenditureReportByYear::class, 'CategorySelected'])->name('CategorySelected')->middleware("auth");

Route::post('SelectYear', [ExpenditureReportByYear::class, 'SelectYear'])->name('SelectYear')->middleware("auth");

Route::get('DeclineLeaveLogic/{id}', [LeaveController::class, 'DeclineLeaveLogic'])->name('DeclineLeaveLogic')->middleware("auth");

Route::get('ApproveLeaveLogic/{id}', [LeaveController::class, 'ApproveLeaveLogic'])->name('ApproveLeaveLogic')->middleware("auth");

Route::get('ApproveLeave', [LeaveController::class, 'ApproveLeave'])->name('ApproveLeave')->middleware("auth");

Route::get('TerminateLeaveApp/{id}', [LeaveController::class, 'TerminateLeaveApp'])->name('TerminateLeaveApp')->middleware("auth");

Route::post('SubmitLeaveApplication', [LeaveController::class, 'SubmitLeaveApplication'])->name('SubmitLeaveApplication')->middleware("auth");

Route::get('ApplyForLeave', [LeaveController::class, 'ApplyForLeave'])->name('ApplyForLeave')->middleware("auth");

Route::post('ViewAssignedSupervisors', [HumanResourceController::class, 'ViewAssignedSupervisors'])->name('ViewAssignedSupervisors')->middleware("auth");

Route::get('RevokeAssignedSupervisor/{id}', [HumanResourceController::class, 'RevokeAssignedSupervisor'])->name('RevokeAssignedSupervisor')->middleware("auth");

Route::post('AssignSupervisorLogic', [HumanResourceController::class, 'AssignSupervisorLogic'])->name('AssignSupervisorLogic')->middleware("auth");

Route::get('ManageSupervisors', [HumanResourceController::class, 'ManageSupervisors'])->name('ManageSupervisors')->middleware("auth");

Route::get('AssignSupervisor', [HumanResourceController::class, 'AssignSupervisor'])->name('AssignSupervisor')->middleware("auth");

Route::get('RevokeSupervisor/{id}', [HumanResourceController::class, 'RevokeSupervisor'])->name('RevokeSupervisor')->middleware("auth");

Route::post('CreateSupervisor', [HumanResourceController::class, 'CreateSupervisor'])->name('CreateSupervisor')->middleware("auth");

Route::get('RunCacheEmployees', [CacheUsersController::class, 'RunCacheEmployees'])->name('RunCacheEmployees')->middleware("auth");

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
